Omid Test Project
server side
>- install mysql
>- create user omid identified by omid@123
>- create database omid
>- grant all privileges on omid.* to omid
>- git clone https://gitlab.com/smzerehpoush/omid.git
>- cd omid/omid-server
>- make sure your vpn is connected
>- make sure port 9898 is free
>- kill -9 $(lsof -t -i:9898)
>- run project using "mvn spring-boot:run"

client side :
> - git clone https://gitlab.com/smzerehpoush/omid-client.git
>- cd omid-client
>- yarn install
>- make sure your port 3000 is free
>- kill -9 $(lsof -t -i:3000)
>- yarn start

good luck :)

