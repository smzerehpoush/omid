package com.mahdiyar.omid.server.controller;

import com.mahdiyar.omid.server.exceptions.*;
import com.mahdiyar.omid.server.model.RestResponse;
import com.mahdiyar.omid.server.model.dto.request.CreateCountryRequestDto;
import com.mahdiyar.omid.server.model.dto.request.EditCountryRequestDto;
import com.mahdiyar.omid.server.model.dto.response.*;
import com.mahdiyar.omid.server.model.entity.CountryEntity;
import com.mahdiyar.omid.server.model.enums.UserRole;
import com.mahdiyar.omid.server.security.AuthRequired;
import com.mahdiyar.omid.server.services.CountryService;
import com.mahdiyar.omid.server.services.MessageService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by mahdiyar on 7/14/18.
 */
@RestController
@RequestMapping("/api/baseInfo")
public class BaseInfoController {
    @Autowired
    private MessageService messageService;
    @Autowired
    private CountryService countryService;

    @ApiOperation(value = "return list of countries")
    @GetMapping("/countries")
    public RestResponse<GetAllCountriesResponseDto> getAllCountries() {
        List<CountryEntity> countries = countryService.findAll();
        return RestResponse.ok(new GetAllCountriesResponseDto(countries));
    }

    @ApiOperation(value = "return list of country names")
    @GetMapping("/countries/names")
    public RestResponse<GetAllCountryNamesResponseDto> getAllCountryNames() {
        List<CountryEntity> countries = countryService.findAll();
        return RestResponse.ok(new GetAllCountryNamesResponseDto(countries));
    }

    @ApiOperation(value = "Create new Country", notes = "Admin Auth Required")
    @PostMapping("/countries")
    @AuthRequired(roles = UserRole.Admin)
    public RestResponse<CreateCountryResponseDto> createCountry(@RequestBody CreateCountryRequestDto request) throws InsufficientDataProvidedException, CountryAlreadyExistException, MediaNotFoundException, InvalidDataException {
        CountryEntity countryEntity = countryService.addNewCountry(request.getName(), request.getFlagPhotoMediaId(), request.getPreCode(), request.isSmsActive());
        return RestResponse.ok(new CreateCountryResponseDto(countryEntity));
    }

    @ApiOperation(value = "Update Country by ID", notes = "Admin Auth Required")
    @PostMapping("/countries/{countryId}")
    @AuthRequired(roles = UserRole.Admin)
    public RestResponse<EditCountryResponseDto> editCountry(@PathVariable("countryId") String countryId, @RequestBody EditCountryRequestDto request) throws CountryNotFoundException, MediaNotFoundException {
        CountryEntity countryEntity = countryService.editCountry(countryId, request.getName(), request.getFlagPhotoMediaId(), request.getPreCode(), request.isSmsActive());
        return RestResponse.ok(new EditCountryResponseDto(countryEntity));
    }

    @ApiOperation(value = "Delete Country by ID", notes = "Admin Auth Required")
    @DeleteMapping("/countries/{countryId}")
    @AuthRequired(roles = UserRole.Admin)
    public RestResponse<Void> deleteCountry(@PathVariable("countryId") String countryId) throws CountryNotFoundException, DeleteNotPermittedException {
        countryService.deleteCountry(countryId);
        return RestResponse.ok();
    }

    @ApiOperation(value = "Get Country by ID", notes = "Admin Auth Required")
    @GetMapping("/countries/{countryId}")
    @AuthRequired(roles = UserRole.Admin)
    public RestResponse<GetCountryResponseDto> getCountry(@PathVariable("countryId") String countryId) throws CountryNotFoundException {
        CountryEntity countryEntity = countryService.findById(countryId);
        return RestResponse.ok(new GetCountryResponseDto(countryEntity));
    }
}
