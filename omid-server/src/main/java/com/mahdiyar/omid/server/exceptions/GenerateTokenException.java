package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by pc on 7/1/2017.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.GenerateToken, description = "Unknown exception while generating auth token")
public class GenerateTokenException extends ServiceException {
    public GenerateTokenException(Throwable cause) {
        super("Failed to generate token", cause);
    }
}
