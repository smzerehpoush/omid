package com.mahdiyar.omid.server.model.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by mhd.zerehpoosh on 8/11/2019.
 */
@Data
@NoArgsConstructor
public class SendSMSResponseDto {
    private String code;
    private String text;
    private String subService;
}
