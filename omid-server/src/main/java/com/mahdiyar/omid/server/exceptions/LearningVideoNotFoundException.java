package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by mahdiyar on 10/20/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.LearningVideoNotFound)
public class LearningVideoNotFoundException extends ServiceException {
    public LearningVideoNotFoundException() {
        super("learning video not found");
    }
}
