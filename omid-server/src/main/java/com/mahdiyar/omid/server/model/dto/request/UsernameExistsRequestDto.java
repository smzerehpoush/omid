package com.mahdiyar.omid.server.model.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by mahdiyar on 7/14/18.
 */
@Data
@NoArgsConstructor
public class UsernameExistsRequestDto {
    @ApiModelProperty(required = true)
    private String username;
}
