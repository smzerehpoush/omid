package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;
import lombok.Getter;

/**
 * Created by mahdiyar on 10/26/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.DeleteNotPermitted, params = "itemName")
public class DeleteNotPermittedException extends ServiceException {
    @Getter
    private final String itemName;

    public DeleteNotPermittedException(String itemName) {
        super(itemName + "cannot be deleted");
        this.itemName = itemName;
    }
}
