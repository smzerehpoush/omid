package com.mahdiyar.omid.server.model.dto.response;

import com.mahdiyar.omid.server.model.dto.UserDto;
import com.mahdiyar.omid.server.model.entity.UserEntity;
import com.mahdiyar.omid.server.services.MessageService;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
/*
    created by mahdiyar
 */

@Data
@NoArgsConstructor
public class SearchByUsernameResponseDto {
    List<UserDto> userList;

    public SearchByUsernameResponseDto(List<UserEntity> userEntityList, MessageService messageService) {
        List<UserDto> userList = new ArrayList<>();
        for (UserEntity user : userEntityList) {
            userList.add(new UserDto(user, messageService));
        }
        this.userList = userList;
    }
}
