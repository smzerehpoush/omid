package com.mahdiyar.omid.server.model.entity;

import com.mahdiyar.omid.server.model.enums.SmsSendStatus;
import com.mahdiyar.omid.server.model.enums.SmsType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by mahdiyar on 6/6/18.
 */
@Entity
@Table(name = "sms_log")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class SmsLogEntity extends BaseEntity {
    @Column(name = "mobile_no", nullable = false)
    private String mobileNo;
    @Column(name = "text", columnDefinition = "TEXT NOT NULL")
    private String text;
    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private SmsType type;
    @Column(name = "send_status")
    private SmsSendStatus status;
    @Column(name = "details")
    private String details;
}
