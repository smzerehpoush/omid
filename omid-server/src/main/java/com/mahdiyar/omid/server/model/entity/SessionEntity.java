package com.mahdiyar.omid.server.model.entity;

import com.mahdiyar.omid.server.model.enums.Platform;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by mahdiyar on 6/9/18.
 */
@Entity
@Table(name = "session")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class SessionEntity extends BaseEntity {
    @Column(name = "token", nullable = false, unique = true)
    private String token;
    @Column(name = "expiration_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expirationDate;
    @Column(name = "push_id")
    private String pushId;
    @Column(name = "device_info", nullable = false)
    private String deviceInfo;
    @Column(name = "device_id", nullable = false)
    private String deviceId;
    @Enumerated(EnumType.STRING)
    @Column(name = "platform")
    private Platform platform;
    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id")
    private UserEntity user;
    @Column(name = "ip")
    private String ip;
    @ManyToOne

    @JoinColumn(name = "version_id")
    private VersionEntity version;
}
