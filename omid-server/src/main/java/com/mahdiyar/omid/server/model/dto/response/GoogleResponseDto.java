package com.mahdiyar.omid.server.model.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * Created by mahdiyar on 6/21/18.
 */
@Data
@NoArgsConstructor
public class GoogleResponseDto {
    String bio;
    Boolean verified;
    private String token;
    private String username;
    private String fullname;
    private String id;
    private Map<String, String> tags;

    public GoogleResponseDto(String token, String username, String fullname, String id, Map<String, String> tags, String bio, Boolean verified) {
        this.token = token;
        this.username = username;
        this.fullname = fullname;
        this.tags = tags;
        this.id = id;
        this.bio = bio;
        this.verified = verified;
    }
}
