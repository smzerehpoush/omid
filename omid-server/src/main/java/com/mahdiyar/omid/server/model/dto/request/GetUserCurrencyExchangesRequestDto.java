package com.mahdiyar.omid.server.model.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Seyyed Mahdiyar Zerehpoush
 */
@Data
@NoArgsConstructor
public class GetUserCurrencyExchangesRequestDto {
    private String token;
}
