package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

import java.io.IOException;

/**
 * Created by mahdiyar on 6/22/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.MediaUpload)
public class MediaUploadException extends ServiceException {
    public MediaUploadException(String absolutePath, IOException e) {
        super("failed to upload file in [" + absolutePath + "]", e);
    }
}
