package com.mahdiyar.omid.server.model.entity;

import com.mahdiyar.omid.server.model.enums.Platform;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by mahdiyar on 11/7/18.
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "version", uniqueConstraints = @UniqueConstraint(columnNames = {"platform", "build_no"}))
@Data
@NoArgsConstructor
public class VersionEntity extends BaseEntity {
    @Column(name = "version", nullable = false)
    private String version;
    @Enumerated(EnumType.STRING)
    @Column(name = "platform", nullable = false)
    private Platform platform;
    @Column(name = "build_no", nullable = false)
    private Long buildNo;
    @Column(name = "published", nullable = false)
    private boolean published;
    @Column(name = "active", nullable = false)
    private boolean active;
    @Column(name = "release_note", columnDefinition = "text")
    private String releaseNote;
}
