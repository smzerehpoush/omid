package com.mahdiyar.omid.server.model.dto;

import com.mahdiyar.omid.server.model.entity.UserEntity;
import com.mahdiyar.omid.server.model.enums.UserRole;
import com.mahdiyar.omid.server.services.MessageService;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Created by mahdiyar on 6/21/18.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class UserDto extends BriefUserDto {

    private UserRole role;
    private CountryDto country;

    public UserDto(UserEntity userEntity, MessageService messageService) {
        super(userEntity);
        this.role = userEntity.getRole();
        this.country = userEntity.getCountry() == null ? null : new CountryDto(userEntity.getCountry());
    }
}
