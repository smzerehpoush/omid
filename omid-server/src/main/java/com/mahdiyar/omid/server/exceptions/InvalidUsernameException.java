package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by mahdiyar on 7/9/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.InvalidUsername)
public class InvalidUsernameException extends ServiceException {

    private final String username;

    public InvalidUsernameException(String username) {
        super("Invalid Username [" + username + "]");
        this.username = username;
    }

    public String getUsername() {
        return username;
    }
}
