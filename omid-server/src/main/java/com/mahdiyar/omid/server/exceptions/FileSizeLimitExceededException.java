package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by mahdiyar on 6/22/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.FileSizeLimitExceeded, params = {"maxFileSizeMb"})
public class FileSizeLimitExceededException extends ServiceException {
    private final int maxFileSizeMb;

    public FileSizeLimitExceededException(int maxFileSizeMb) {
        super("Uploaded file size reached the limit of [maxFileSizeMb = " + maxFileSizeMb + "]");
        this.maxFileSizeMb = maxFileSizeMb;
    }

    public int getMaxFileSizeMb() {
        return maxFileSizeMb;
    }
}
