package com.mahdiyar.omid.server.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by mhd.zerehpoosh on 8/13/2019.
 */
@Embeddable
@Data
@NoArgsConstructor
@Entity
@Table(name = "address")
public class AddressEntity extends BaseEntity {
    @Column(name = "address")
    private String address;
    @Column(name = "latitude")
    private Double latitude;
    @Column(name = "longitude")
    private Double longitude;
}
