package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by mahdiyar on 10/22/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.UserNotValidated)
public class UserNotValidatedException extends ServiceException {

    public UserNotValidatedException() {
        super("User is not validated");
    }
}
