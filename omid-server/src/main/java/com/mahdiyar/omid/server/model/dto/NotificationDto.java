package com.mahdiyar.omid.server.model.dto;

import com.mahdiyar.omid.server.model.entity.NotificationEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by mahdiyar on 11/19/18.
 */
@Data
@NoArgsConstructor
public class NotificationDto {
    private String id;
    private long creationDate;
    private String title;
    private String subTitle;
    private String body;

    public NotificationDto(NotificationEntity notificationEntity) {
        this.id = notificationEntity.getId();
        this.creationDate = notificationEntity.getCreationDate().getTime();
        this.title = notificationEntity.getTitle();
        this.subTitle = notificationEntity.getSubTitle();
        this.body = notificationEntity.getBody();
    }

}
