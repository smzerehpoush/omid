package com.mahdiyar.omid.server.services;

import com.mahdiyar.omid.server.dao.SmsLogRepo;
import com.mahdiyar.omid.server.model.entity.SmsLogEntity;
import com.mahdiyar.omid.server.model.enums.SmsSendStatus;
import com.mahdiyar.omid.server.model.enums.SmsType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by mahdiyar on 6/6/18.
 */
@Service
public class SmsLogService {

    private static final Logger logger = LoggerFactory.getLogger(SmsLogService.class);

    @Autowired
    private SmsLogRepo smsLogRepo;

    public void saveNewLog(String mobileNo, String text, SmsType type, SmsSendStatus status, String details) {
        logger.info("trying to save sms log [mobileNo: " + mobileNo + ", text: " + text + ", type: " + type + ", status: " + status + ", details: " + details + "]");
        SmsLogEntity smsLogEntity = new SmsLogEntity();
        smsLogEntity.setMobileNo(mobileNo);
        smsLogEntity.setText(text);
        smsLogEntity.setType(type);
        smsLogEntity.setStatus(status);
        smsLogEntity.setDetails(details);
        logger.info("sms log successfully saved: [id: " + smsLogEntity.getId() + "]");
        smsLogRepo.save(smsLogEntity);
    }

    public void initializeSmsLogs() {

    }
}

