package com.mahdiyar.omid.server.model.dto.response;

import com.mahdiyar.omid.server.model.dto.VersionDto;
import com.mahdiyar.omid.server.model.entity.VersionEntity;
import lombok.Data;

/**
 * Created by mahdiyar on 11/7/18.
 */
@Data
public class CreateVersionResponseDto extends VersionDto {
    public CreateVersionResponseDto(VersionEntity versionEntity) {
        super(versionEntity);
    }
}
