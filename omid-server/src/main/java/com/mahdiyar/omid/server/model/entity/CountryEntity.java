package com.mahdiyar.omid.server.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by mahdiyar on 8/13/18.
 */
@Entity
@Table(name = "country")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class CountryEntity extends BaseEntity {
    @Column(name = "country_name", nullable = false, unique = true)
    private String name;
    @Column(name = "pre_code", unique = true)
    private String preCode;
    @Column(name = "is_sms_active", nullable = false)
    private boolean smsActive;
}
