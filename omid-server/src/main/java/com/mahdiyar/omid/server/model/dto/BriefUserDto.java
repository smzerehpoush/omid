package com.mahdiyar.omid.server.model.dto;

import com.mahdiyar.omid.server.model.entity.UserEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by mahdiyar on 10/20/18.
 */
@Data
@NoArgsConstructor
public class BriefUserDto {
    private String id;
    private String username;
    private String fullName;
    private Boolean active;
    private String shareLink;
    private String bio;
    private Boolean verified;

    public BriefUserDto(UserEntity userEntity) {
        this.id = userEntity.getId();
        this.username = userEntity.getUsername();
        this.fullName = userEntity.getFullName();
        this.active = userEntity.isActive();
        this.shareLink = userEntity.getShareLink();
        this.bio = userEntity.getBio();
        this.verified = userEntity.getVerified();
    }
}
