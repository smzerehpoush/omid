package com.mahdiyar.omid.server.model.dto.response;

import com.mahdiyar.omid.server.model.dto.CountryDto;
import com.mahdiyar.omid.server.model.entity.CountryEntity;
import lombok.Data;

/**
 * Created by mahdiyar on 11/9/18.
 */
@Data
public class CreateCountryResponseDto extends CountryDto {
    public CreateCountryResponseDto(CountryEntity countryEntity) {
        super(countryEntity);
    }
}
