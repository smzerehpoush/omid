package com.mahdiyar.omid.server.exceptions;

import java.rmi.ServerException;

//@HandledException(enumerator = ServiceExceptionEnumerator.SportNotFoundException)
public class SportNotFoundException extends ServerException {
    public SportNotFoundException() {
        super("Sport not found exception");
    }
}
