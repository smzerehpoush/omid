package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by mahdiyar on 8/16/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.ResortNotFound)
public class ResortNotFoundException extends ServiceException {
    public ResortNotFoundException(String id) {
        super("Resort by id [" + id + "] not found");
    }
}
