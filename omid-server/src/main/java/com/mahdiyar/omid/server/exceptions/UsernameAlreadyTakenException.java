package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by mahdiyar on 7/9/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.UsernameAlreadyTakenException)
public class UsernameAlreadyTakenException extends ServiceException {
    private final String username;

    public UsernameAlreadyTakenException(String username) {
        super("username [" + username + "] already taken");
        this.username = username;
    }

    public String getUsername() {
        return username;
    }
}
