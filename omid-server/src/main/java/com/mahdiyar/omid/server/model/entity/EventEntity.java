package com.mahdiyar.omid.server.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by mhd.zerehpoosh on 8/13/2019.
 */
@Table(name = "event")
@Entity
@Data
@EqualsAndHashCode
@NoArgsConstructor
public class EventEntity extends BaseEntity {
    @OneToOne
    @JoinColumn(name = "front_pic_id")
    private PictureEntity frontPic;
    @OneToOne
    @JoinColumn(name = "rear_pic_id")
    private PictureEntity rearPic;
    @OneToOne
    @JoinColumn(name = "plate_pic_id")
    private PictureEntity platePic;
    @OneToOne
    @JoinColumn(name = "address_id")
    private AddressEntity address;
    @Column(name = "caption")
    private String caption;

}
