package com.mahdiyar.omid.server.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @author Seyyed Mahdiyar Zerehpoush
 */
@Entity
@Table(name = "user_currency")
@Data
@NoArgsConstructor
public class UserCurrencyEntity extends BaseEntity{
    @OneToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;
    @OneToOne
    @JoinColumn(name = "from_country")
    private CountryEntity from;
    @OneToOne
    @JoinColumn(name = "to_country")
    private CountryEntity to;
}
