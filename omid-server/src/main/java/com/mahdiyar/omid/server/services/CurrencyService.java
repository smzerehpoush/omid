package com.mahdiyar.omid.server.services;

import com.mahdiyar.omid.server.exceptions.CountryNotFoundException;
import com.mahdiyar.omid.server.exceptions.FixerAPIException;
import com.mahdiyar.omid.server.model.dto.response.RatesResponseDto;
import com.mahdiyar.omid.server.model.entity.CountryEntity;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

/**
 * Created by mhd.zerehpoosh on 8/17/2019.
 */
@Service
@ConfigurationProperties(prefix = "com.mahdiyar.omid.server.services.currency-service")
public class CurrencyService {
    @Setter
    private String apiKey;
    @Autowired
    private CountryService countryService;
    @Autowired
    private CountryExchangeRateService countryExchangeRateService;

    private static final Logger logger = LoggerFactory.getLogger(CurrencyService.class);

    public void initializeCurrencies() {
        RestTemplate restTemplate = new RestTemplate();
        try {
            URI uri = new URI("http://data.fixer.io/api/latest?access_key=" + apiKey + "&format=1");
            RatesResponseDto responseDto = restTemplate.getForObject(uri, RatesResponseDto.class);
            if (!responseDto.isSuccess())
                throw new FixerAPIException();
            for (String countryName : responseDto.getRates().keySet()) {
                CountryEntity countryEntity = countryService.addNewCountry(countryName);
                countryExchangeRateService.addNewCountryExchangeRate(countryEntity, responseDto.getRates().get(countryName));
            }
            System.out.println(responseDto);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
//            throw new FixerAPIException();
        }
    }

    public double convertCurrencyRate(String from, String to) throws CountryNotFoundException {
        Double fromRate = countryExchangeRateService.getRate(from);
        Double toRate = countryExchangeRateService.getRate(to);
        if (fromRate.equals(0d))
            return 0d;
        return toRate / fromRate;
    }
}
