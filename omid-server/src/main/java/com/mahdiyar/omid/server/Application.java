package com.mahdiyar.omid.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * Created by mahdiyar on 6/29/18.
 */
@SpringBootApplication
public class Application {
    private static ApplicationContext applicationContext;

    private static boolean contextLoaded = false;

    public static void main(String[] args) {
        applicationContext = SpringApplication.run(Application.class, args);
    }

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public static void contextLoaded() {
        contextLoaded = true;
    }

    public static boolean isContextLoaded() {
        return contextLoaded;
    }

    public static void shutdown() {
        ((ConfigurableApplicationContext) applicationContext).close();
    }
}
