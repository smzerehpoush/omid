package com.mahdiyar.omid.server.model.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Seyyed Mahdiyar Zerehpoush
 */
@Data
@NoArgsConstructor
public class RemoveCurrencyRequestDto {
    private String from;
    private String to;
    private String token;
}
