package com.mahdiyar.omid.server.security;

import com.mahdiyar.omid.server.model.RequestContext;
import com.mahdiyar.omid.server.model.entity.SessionEntity;
import com.mahdiyar.omid.server.services.SessionService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

/**
 * Created by pc on 7/16/2017.
 */
@Component
public class RestApiAuthenticationInterceptor extends HandlerInterceptorAdapter {
    //    public static final String HEADER_X_PLATFORM = "X-Platform";
//    public static final String HEADER_X_VERSION = "X-Version";
//    public static final String HEADER_X_BUILDNO = "X-BuildNo";
//    public static final String HEADER_X_LANGUAGE = "X-Language";
    public static final String COOKIE_TOKEN = "token";
    private static final Logger logger = LoggerFactory.getLogger(RestApiAuthenticationInterceptor.class);
    @Autowired
    private SessionService sessionService;
    @Autowired
    private RequestContext requestContext;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (request.getMethod().equals("OPTIONS")) {
            response.addHeader("Access-Control-Allow-Headers", "*");
        }
        resolveClientIP(request);
        resolveTokenInfo(request);
        requestContext.setRequest(request);
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            Method method = handlerMethod.getMethod();

            if (method.isAnnotationPresent(AuthRequired.class)) {
                AuthRequired authRequired = method.getAnnotation(AuthRequired.class);
                SessionEntity sessionEntity = sessionService.validateToken(requestContext.getTokenStr(), authRequired.roles(), requestContext.getClientIp());
                requestContext.setSession(sessionEntity);
            }

        }
        return true;
    }

    private void resolveClientIP(HttpServletRequest request) {
        String remoteAddr = "";

        if (request != null) {
            remoteAddr = request.getHeader("X-FORWARDED-FOR");
            if (StringUtils.isEmpty(remoteAddr)) {
                remoteAddr = request.getRemoteAddr();
            }
        }
        requestContext.setClientIp(remoteAddr);
    }

    private void resolveTokenInfo(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie : cookies) {
                if (COOKIE_TOKEN.equals(cookie.getName())) {
                    String tokenValue = cookie.getValue();
                    if (!StringUtils.isEmpty(tokenValue)) {
                        requestContext.setTokenStr(tokenValue);
                    } else {
                        logger.info("token value is empty");
                    }
                    break;
                }
            }
        }
    }
}
