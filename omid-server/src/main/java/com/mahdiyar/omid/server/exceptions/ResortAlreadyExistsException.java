package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by mahdiyar on 9/12/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.ResortAlreadyExists)
public class ResortAlreadyExistsException extends ServiceException {
    public ResortAlreadyExistsException(String name) {
        super("Resort by name " + name + " already exists");
    }
}
