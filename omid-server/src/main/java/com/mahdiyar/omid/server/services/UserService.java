package com.mahdiyar.omid.server.services;

import com.google.common.collect.ImmutableMap;
import com.mahdiyar.omid.server.dao.ActivationCodeRepo;
import com.mahdiyar.omid.server.dao.UserCurrencyRepo;
import com.mahdiyar.omid.server.dao.UserRepo;
import com.mahdiyar.omid.server.exceptions.*;
import com.mahdiyar.omid.server.exceptions.handler.ServiceExceptionHandler;
import com.mahdiyar.omid.server.model.dto.request.AddNewCurrencyRequestDto;
import com.mahdiyar.omid.server.model.dto.request.RemoveCurrencyRequestDto;
import com.mahdiyar.omid.server.model.dto.response.SearchByUsernameResponseDto;
import com.mahdiyar.omid.server.model.dto.response.UserCurrencyResponseDto;
import com.mahdiyar.omid.server.model.dto.response.UsernameExistsResponseDto;
import com.mahdiyar.omid.server.model.entity.*;
import com.mahdiyar.omid.server.model.enums.Platform;
import com.mahdiyar.omid.server.model.enums.SmsType;
import com.mahdiyar.omid.server.model.enums.UserRole;
import com.mahdiyar.omid.server.utils.GeneralUtils;
import com.mahdiyar.omid.server.utils.Pair;
import com.mahdiyar.omid.server.utils.PasswordAuthentication;
import com.mahdiyar.omid.server.utils.PhoneUtils;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by mahdiyar on 6/6/18.
 */
@Service
@ConfigurationProperties(prefix = "com.mahdiyar.omid.server.services.user-service")
public class UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserService.class);
    private static final String SYSADMIN_USERNAME = "sysadmin";
    private static final String SYSADMIN_DEFAULT_PASSWORD = "b1@t@gh@Mef@rd@nakh0r1M";
    private static final String SYSADMIN_MOBILENO = "09121111111";
    private static final String SYSADMIN_FULLNAME = "System Administrator";
    @Setter
    private int activationCodeLength;
    @Setter
    private int activationCodeMaxTryCount;
    @Setter
    private int activationCodeMaxLifeTimeMinutes;
    @Setter
    private String demoUserMobileNo;
    @Setter
    private String demoUserActivationCode;
    @Setter
    private String usernamePattern;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private ActivationCodeRepo activationCodeRepo;
    @Autowired
    private SmsService smsService;
    @Autowired
    private MessageService messageService;
    @Autowired
    private SessionService sessionService;
    @Autowired
    private CountryService countryService;
    @Autowired
    private UserCurrencyRepo userCurrencyRepo;
    @Autowired
    private CurrencyService currencyService;
    private PasswordAuthentication passwordAuthentication = new PasswordAuthentication(PasswordAuthentication.DEFAULT_COST);

    public UserEntity signupByEmailAndPassword(String email, String password, String mobileNo, String countryId, UserRole role) throws InvalidDataException, UsernameAlreadyTakenException, InsufficientDataProvidedException, CountryNotFoundException, UserByMobileNoAlreadyExists, UserByEmailAlreadyExists {
        if (StringUtils.isEmpty(password)) {
            throw new InsufficientDataProvidedException("password", "signup");
        }
        if (!StringUtils.isEmpty(email))
            if (!GeneralUtils.validateEmail(email)) {
                throw new InvalidDataException("email", "signup");
            }
        CountryEntity country = StringUtils.isEmpty(countryId) ? countryService.findByName("Iran") : countryService.findById(countryId);
        String plainMobileNo = null;
        if (!StringUtils.isEmpty(mobileNo)) {
            if (!PhoneUtils.validateMobileNo(country.getPreCode(), mobileNo)) {
                throw new InvalidDataException("mobileNo", "signup");
            }
            plainMobileNo = PhoneUtils.toPlain(country.getPreCode(), mobileNo);
        }

        try {
            if (!StringUtils.isEmpty(mobileNo)) {
                UserEntity duplicate = findUserEntityByMobileNo(plainMobileNo);
                logger.info("user by mobileNo [" + plainMobileNo + "] already exists.");
                throw new UserByMobileNoAlreadyExists(plainMobileNo);
            }
        } catch (UserNotFoundException e) {
            //nothing
        }
        if (userRepo.existsByEmail(email)) {
            throw new UserByEmailAlreadyExists(email);
        }
        UserEntity userEntity = new UserEntity();
        userEntity.setMobileNo(plainMobileNo);
        userEntity.setEmail(email);
        userEntity.setHashedPassword(passwordAuthentication.hash(password.toCharArray()));
        userEntity.setCountry(country);
        userEntity.setRole(role);
        userEntity.setValidated(false);

        userRepo.save(userEntity);

        // TODO: 10/22/18 send validation email

        return userEntity;

    }

    public UserEntity signupByUsernameAndPassword(String username, String password, String mobileNo, String countryId, UserRole role) throws InvalidDataException, UsernameAlreadyTakenException, InsufficientDataProvidedException, CountryNotFoundException, UserByMobileNoAlreadyExists, UserByEmailAlreadyExists {
        if (StringUtils.isEmpty(password)) {
            throw new InsufficientDataProvidedException("password", "signup");
        }
        CountryEntity country = StringUtils.isEmpty(countryId) ? countryService.findByName("Iran") : countryService.findById(countryId);
        String plainMobileNo = null;
        if (!StringUtils.isEmpty(mobileNo)) {
            if (!PhoneUtils.validateMobileNo(country.getPreCode(), mobileNo)) {
                throw new InvalidDataException("mobileNo", "signup");
            }
            plainMobileNo = PhoneUtils.toPlain(country.getPreCode(), mobileNo);
        }

        UserEntity user = userRepo.findDistinctByUsername(username);
        if (user != null)
            throw new UsernameAlreadyTakenException(username);

        UserEntity userEntity = new UserEntity();
        userEntity.setMobileNo(plainMobileNo);
        userEntity.setUsername(username);
        userEntity.setHashedPassword(passwordAuthentication.hash(password.toCharArray()));
        userEntity.setCountry(country);
        userEntity.setRole(role);
        userEntity.setValidated(true);

        userRepo.save(userEntity);

        // TODO: 10/22/18 send validation email

        return userEntity;

    }

    public ActivationCodeEntity signupByMobileNo(String mobileNo, String countryId, UserRole userRole) throws InvalidDataException, InsufficientDataProvidedException, SendSmsException, UsernameAlreadyTakenException, InvalidUsernameException, CountryNotFoundException {
        logger.info("signup request received for mobileNo: [" + mobileNo + "]");
        if (StringUtils.isEmpty(mobileNo)) {
            throw new InsufficientDataProvidedException("mobileNo", "singUp");
        }
        CountryEntity country;
        if (countryId == null) {
            country = countryService.findByName("Iran");
        } else
            country = countryService.findById(countryId);
        UserEntity userEntity;

        if (!PhoneUtils.validateMobileNo(country.getPreCode(), mobileNo)) {
            throw new InvalidDataException("mobileNo", "signUp");
        }
        /*if (!validateUsername(username)) {
            throw new InvalidUsernameException(username);
        }*/

        String plainMobileNo = PhoneUtils.toPlain(country.getPreCode(), mobileNo);
        try {
            userEntity = findUserEntityByMobileNo(plainMobileNo);
            logger.info("user by mobileNo [" + plainMobileNo + "] is already exists.");
            /*try {
                UserEntity sameUserNameEntity = findUserByUsername(username);
                if (!Objects.equals(sameUserNameEntity.getMobileNo(), plainMobileNo)) {
                    throw new UsernameAlreadyTakenException(username);
                }
            } catch (UserNotFoundException e) {
                //ignored
            }*/
        } catch (UserNotFoundException e) {
            logger.info("creating user for mobileNo [" + mobileNo + "]...");
            userEntity = new UserEntity();
            userEntity.setMobileNo(plainMobileNo);
            userEntity.setCountry(country);
            userEntity.setRole(userRole);
//            userEntity.setUsername(username);
            userEntity.setValidated(false);
            userEntity = userRepo.save(userEntity);
            logger.info("user created for mobileNo [" + plainMobileNo + "]: id: [" + userEntity.getId() + "]");
//            List<UserEntity> usersInvitedMobileNo = invitationService.findUserEntitiesInvitedMobileNo(mobileNo);
        }

        boolean sendSMS = !plainMobileNo.equals(demoUserMobileNo);

        ActivationCodeEntity activationCodeEntity;
        boolean createNewActivationCode = true;
        activationCodeEntity = activationCodeRepo.findTopByUserOrderByCreationDateDesc(userEntity);
        if (activationCodeEntity != null) {
            if (!activationCodeEntity.isConsumed() && !isActivationCodeExpired(activationCodeEntity) && !isActivationCodeTryCountExceededTheMaxTryCount(activationCodeEntity)) {
                createNewActivationCode = false;
            }
        }
        if (createNewActivationCode) {
            activationCodeEntity = new ActivationCodeEntity();
            activationCodeEntity.setUser(userEntity);
            if (plainMobileNo.equals(demoUserMobileNo)) {
                activationCodeEntity.setCode(demoUserActivationCode);
            } else {
                activationCodeEntity.setCode(GeneralUtils.generateRandomDigits(activationCodeLength));
            }
            activationCodeEntity.setConsumed(false);
            activationCodeEntity.setTriesCount(0);
            activationCodeRepo.save(activationCodeEntity);
        }
        if (sendSMS) {
            logger.info("Sending Activation Code for " + plainMobileNo + ", activationCode: " + activationCodeEntity.getCode());
            smsService.sendSms(
                    messageService.getMessage("signupwelcomemessage", ImmutableMap.of("activationCode", activationCodeEntity.getCode())),
//                    messageService.getMessage("signupwelcomemessage", ImmutableMap.of("activationCode", "*****")),
                    messageService.getMessage("signupwelcomemessage", ImmutableMap.of("activationCode", activationCodeEntity.getCode())),
                    userEntity,
                    SmsType.ActivationCode,
                    activationCodeEntity.getCode());
        } else {
            logger.info("No need for sending activation code for " + plainMobileNo + ", the activation code is : " + activationCodeEntity.getCode());
        }
        return activationCodeEntity;
    }

    private boolean validateUsername(String username) {
        Pattern pattern = Pattern.compile(usernamePattern);
        Matcher matcher = pattern.matcher(username);
        return matcher.matches();
    }

    public UserEntity findUserByUsername(String username) throws UserNotFoundException {
        UserEntity userEntity = userRepo.findDistinctByUsername(username);
        if (userEntity == null) {
            throw UserNotFoundException.byUsername(username);
        }
        return userEntity;
    }

    public Pair<SessionEntity, String> loginByUsernameAndPassword(String username, String password, String deviceId, String deviceInfo, String ip, Platform platform, Long buildNo) throws UserNotFoundException, InsufficientDataProvidedException, WrongPasswordException, GenerateTokenException, UserNotValidatedException, VersionNotFoundException {
        if (StringUtils.isEmpty(username)) {
            throw new InsufficientDataProvidedException("username", "loginByUsernameAndPassword");
        }
        if (StringUtils.isEmpty(password)) {
            throw new InsufficientDataProvidedException("password", "loginByUsernameAndPassword");
        }
        if (platform == null) {
            throw new InsufficientDataProvidedException("platform", "loginByUsernameAndPassword");
        }
        if (buildNo == null) {
            throw new InsufficientDataProvidedException("buildNo", "loginByUsernameAndPassword");
        }
        UserEntity userEntity = findUserByUsername(username);
        if (!passwordAuthentication.authenticate(password.toCharArray(), userEntity.getHashedPassword())) {
            throw new WrongPasswordException();
        }

        if (!userEntity.isValidated()) {
            throw new UserNotValidatedException();
        }

        return sessionService.createSessionEntityForUser(userEntity, deviceId, deviceInfo, platform, buildNo, ip);

    }

    public Pair<SessionEntity, String> validateUserByActivationCode(String mobileNo, String countryId, String activationCode, String deviceId, String deviceInfo, Platform platform, Long buildNo, String ip) throws UserNotFoundException, NoActivationCodeGeneratedException, ConsumedActivationCodeException, ExpiredActivationCodeException, ActivationCodeMismatchException, GenerateTokenException, CountryNotFoundException, InsufficientDataProvidedException, VersionNotFoundException {
        assert !StringUtils.isEmpty(mobileNo);
        assert !StringUtils.isEmpty(activationCode);
        logger.info("trying to activate user [mobileNo: " + mobileNo + ", countryId: " + countryId + ", activationCode: " + activationCode + ", deviceId: " + deviceId + ", deviceInfo: " + deviceInfo + ", platform: " + platform + ", ip: " + ip + "]");
        if (buildNo == null) {
            throw new InsufficientDataProvidedException("buildNo", "validate-user-by-activation-code");
        }
        CountryEntity country = countryId == null ? countryService.findByName("Iran") : countryService.findById(countryId);
        String plainMobileNo = PhoneUtils.toPlain(country.getPreCode(), mobileNo);
        UserEntity userEntity = findUserEntityByMobileNo(plainMobileNo);
        ActivationCodeEntity activationCodeEntity = activationCodeRepo.findTopByUserOrderByCreationDateDesc(userEntity);
//        ActivationCodeEntity activationCodeEntity = activationCodeRepo.findByIdAndUser(activationCodeId, userEntity);
        if (activationCodeEntity == null) {
            logger.info("no activation code generated for user [mobileNo: " + plainMobileNo + "]");
            throw new NoActivationCodeGeneratedException(plainMobileNo, null);
        }
        if (activationCodeEntity.isConsumed()) {
            logger.info("activation code generated for user [mobileNo: " + plainMobileNo + "] is already consumed");
            throw new ConsumedActivationCodeException(plainMobileNo);
        }
        if (isActivationCodeTryCountExceededTheMaxTryCount(activationCodeEntity)) {
            logger.info("try count for activation code exceeded the maximum try count. [activationCodeId: " + activationCodeEntity.getId() + ", triesCount: " + activationCodeEntity.getTriesCount() + "]");
            throw new ExpiredActivationCodeException(plainMobileNo, activationCodeEntity.getId());
        }
        if (isActivationCodeExpired(activationCodeEntity)) {
            logger.info("activation code reached the maximum life time and is not valid anymore. [activationCodeId: " + activationCodeEntity.getId() + ", mobileNo: " + mobileNo + "]");
            throw new ExpiredActivationCodeException(plainMobileNo, activationCodeEntity.getId());
        }
        if (!StringUtils.equals(activationCode, activationCodeEntity.getCode())) {
            manageActivationCodeFailedTry(activationCodeEntity);
            logger.info("activation code mismatch for user [mobileNo: " + plainMobileNo + "]");
            throw new ActivationCodeMismatchException(plainMobileNo, activationCodeEntity.getCode(), activationCode);
        }
        activationCodeEntity.setConsumeDate(new Date());
        activationCodeEntity.setConsumed(true);
        activationCodeRepo.save(activationCodeEntity);

        userEntity.setValidated(true);

        sessionService.expireActiveSessionsByDevice(deviceId);

        Pair<SessionEntity, String> result = sessionService.createSessionEntityForUser(userEntity, deviceId, deviceInfo, platform, buildNo, ip);
        logger.info("user [mobileNo: " + plainMobileNo + "] successfully activated.");
        return result;
    }

    UserEntity findUserEntityByMobileNo(String mobileNo) throws UserNotFoundException {
        assert !StringUtils.isEmpty(mobileNo);
        UserEntity userEntity = userRepo.findByMobileNo(mobileNo);
        if (userEntity == null) {
            throw UserNotFoundException.byMobileNo(mobileNo);
        }
        return userEntity;
    }

    UserEntity findUserEntityByEmail(String email) throws UserNotFoundException {
        assert !StringUtils.isEmpty(email);
        UserEntity userEntity = userRepo.findByEmail(email);
        if (userEntity == null) {
            throw UserNotFoundException.byMobileNo(email);
        }
        return userEntity;
    }

    @Transactional
    public void manageActivationCodeFailedTry(ActivationCodeEntity activationCodeEntity) {
        int triesCount = activationCodeEntity.getTriesCount() != null ? activationCodeEntity.getTriesCount() : 0;
        activationCodeEntity.setTriesCount(triesCount + 1);
        activationCodeRepo.save(activationCodeEntity);
    }

    private boolean isActivationCodeExpired(ActivationCodeEntity activationCodeEntity) {
        return activationCodeEntity.getCreationDate() != null && DateUtils.addMinutes(activationCodeEntity.getCreationDate(), activationCodeMaxLifeTimeMinutes).compareTo(new Date()) < 0;
    }

    private boolean isActivationCodeTryCountExceededTheMaxTryCount(ActivationCodeEntity activationCodeEntity) {
        return activationCodeEntity.getTriesCount() != null && activationCodeEntity.getTriesCount() >= activationCodeMaxTryCount;
    }

    public UserEntity updateProfile(UserEntity user, String username, String fullName, String profilePictureMediaId, String bio) throws MediaNotFoundException, UsernameAlreadyTakenException, InsufficientDataProvidedException, InvalidUsernameException, UsernameChangeNotAllowedException {
        if (StringUtils.isEmpty(user.getUsername()) && StringUtils.isEmpty(username)) {
            throw new InsufficientDataProvidedException("username", "signup");
        }
        if (!StringUtils.isEmpty(username) && !validateUsername(username)) {
            throw new InvalidUsernameException(username);
        }
        // username could not be changed
        /*if (!StringUtils.isEmpty(username) && !StringUtils.isEmpty(user.getUsername()) && !Objects.equals(user.getUsername(), username)) {
            throw new UsernameChangeNotAllowedException(user.getUsername(), username);
        }*/

        if (!StringUtils.isEmpty(username)) {
            try {
                UserEntity sameUserNameEntity = findUserByUsername(username);
                if (!Objects.equals(sameUserNameEntity.getId(), user.getId())) {
                    throw new UsernameAlreadyTakenException(username);
                }
            } catch (UserNotFoundException e) {
                //ignored
            }
            user.setUsername(username);
        }
        user.setFullName(fullName);
        user.setBio(bio);
        userRepo.save(user);
        return user;
    }

    public UserEntity findUserEntityById(String ownerUserId) throws UserNotFoundException {
        UserEntity userEntity = userRepo.findOne(ownerUserId);
        if (userEntity == null) {
            throw UserNotFoundException.byId(ownerUserId);
        }
        return userEntity;
    }

    public UsernameExistsResponseDto checkUsernameExistsForUser(UserEntity user, String username) throws InsufficientDataProvidedException, InvalidUsernameException, UsernameAlreadyTakenException, UserNotFoundException {
        if (user == null)
            throw UserNotFoundException.byId("");
        if (StringUtils.isEmpty(username)) {
            throw new InsufficientDataProvidedException("username", "usernameExists");
        }
        if (!validateUsername(username)) {
            throw new InvalidUsernameException(username);
        }
        try {
            try {
                UserEntity sameUserNameEntity = findUserByUsername(username);
                if (!Objects.equals(sameUserNameEntity.getId(), user.getId())) {
                    throw new UsernameAlreadyTakenException(username);
                }
            } catch (UserNotFoundException e) {
                //ignored
            }
            return new UsernameExistsResponseDto(true, null);
        } catch (Exception ex) {
            ServiceExceptionHandler.ExceptionDetails details = ServiceExceptionHandler.getExceptionDetails(ex, messageService);
            return new UsernameExistsResponseDto(false, details.getMessage());
        }
    }

    public void initializeSysAdminUser() {
        if (!userRepo.existsByUsername(SYSADMIN_USERNAME)) {
            CountryEntity iran = null;
            try {
                iran = countryService.findByName("Iran");
            } catch (CountryNotFoundException ex) {
                ex.printStackTrace();
            }
            try {
                createUser(SYSADMIN_USERNAME, SYSADMIN_DEFAULT_PASSWORD, SYSADMIN_FULLNAME, SYSADMIN_MOBILENO, iran.getId(), UserRole.Admin);
            } catch (UsernameAlreadyTakenException | InsufficientDataProvidedException e) {
                logger.error("error while creating sysadmin");
            } catch (InvalidDataException e) {
                logger.error("Invalid data", e);
            }
        }

    }

    public UserEntity createUser(String username, String password, String fullName, String mobileNo, String countryId, UserRole role) throws UsernameAlreadyTakenException, InsufficientDataProvidedException, InvalidDataException {
        if (StringUtils.isEmpty(username)) {
            throw new InsufficientDataProvidedException("username", "createUser");
        }
        if (StringUtils.isEmpty(password)) {
            throw new InsufficientDataProvidedException("password", "createUser");
        }
        if (StringUtils.isEmpty(mobileNo)) {
            throw new InsufficientDataProvidedException("mobileNo", "createUser");
        }
        if (role == null) {
            throw new InsufficientDataProvidedException("role", "createUser");
        }
        if (userRepo.existsByUsername(username)) {
            throw new UsernameAlreadyTakenException(username);
        }
        UserEntity userEntity = new UserEntity();
        try {
            userEntity.setCountry(!StringUtils.isEmpty(countryId) ? countryService.findById(countryId) : null);
        } catch (CountryNotFoundException e) {
            logger.error("country by id {} not found", countryId);
        }
        if (!PhoneUtils.validateMobileNo(userEntity.getCountry() != null ? userEntity.getCountry().getPreCode() : "98", mobileNo)) {
            throw new InvalidDataException("mobileNo", "signUp");
        }
        userEntity.setUsername(username);
        userEntity.setHashedPassword(passwordAuthentication.hash(password.toCharArray()));
        userEntity.setMobileNo(mobileNo);
        userEntity.setFullName(fullName);
        userEntity.setRole(role);
        userEntity.setValidated(true);
        userRepo.save(userEntity);

        return userEntity;
    }

    public boolean existsByCountry(String countryId) {
        return userRepo.existsByCountry_Id(countryId);
    }

    public List<UserEntity> getUsersByCountryId(String countryId) {
        if (!StringUtils.isEmpty(countryId)) {
            return userRepo.findByCountry_Id(countryId, new Sort(Sort.Direction.ASC, "creationDate"));
        } else {
            return userRepo.findAll(new Sort(Sort.Direction.ASC, "creationDate"));
        }
    }

    public UserEntity activateUser(String userId) throws UserNotFoundException {
        UserEntity userEntity = userRepo.findOne(userId);
        if (userEntity == null) {
            throw UserNotFoundException.byId(userId);
        }
        userEntity.setActive(!userEntity.isActive());
        userRepo.save(userEntity);
        return userEntity;
    }

    public UserEntity updateUserPhoneNumber(String phoneNumber, String newPhoneNumber) throws UserNotFoundException, UserByMobileNoAlreadyExists {
        UserEntity userEntity = userRepo.findByMobileNo(phoneNumber);
        if (userEntity == null) {
            throw UserNotFoundException.byMobileNo(phoneNumber);
        }
        if (userRepo.existsByMobileNo(newPhoneNumber)) {
            throw new UserByMobileNoAlreadyExists(newPhoneNumber);
        }
        userEntity.setMobileNo(newPhoneNumber);
        userRepo.save(userEntity);
        return userEntity;

    }

    private List<UserEntity> findUsersByUsername(String username, CountryEntity country) throws CountryNotFoundException {
        List<UserEntity> userEntityList = userRepo.findAllByUsernameOrderByUsernameDesc(username);
        List<UserEntity> filteredList = new ArrayList<>();
        for (UserEntity user : userEntityList) {
            if (user.getCountry().equals(country))
                filteredList.add(user);
        }
        return filteredList.size() > 15 ? filteredList.subList(0, 14) : filteredList;
    }

    public SearchByUsernameResponseDto searchUsersByUsername(String username, UserEntity userEntity) throws CountryNotFoundException {
        List<UserEntity> userList = findUsersByUsername(username, userEntity.getCountry());
        return new SearchByUsernameResponseDto(userList, messageService);
    }

    public UserEntity findByShareLink(String shareLink) {
        return userRepo.findByShareLink(shareLink);
    }

    public UserEntity findById(String id) throws UserNotFoundException {
        UserEntity user = userRepo.findOne(id);
        if (user == null)
            throw UserNotFoundException.byId(id);
        return user;
    }

    public void initializeUsersShareLink() {
        List<UserEntity> users = userRepo.findAll();
        for (UserEntity user : users) {
            if (user.getShareLink() == null || user.getShareLink().equals("")) {
                user.setShareLink(UUID.randomUUID().toString().substring(0, 8));
                userRepo.save(user);
            }
        }
    }

    public UserCurrencyEntity addNewCurrency(AddNewCurrencyRequestDto request) throws InsufficientDataProvidedException, CountryNotFoundException, SessionNotFoundException {
        UserEntity user = sessionService.findTokenEntityByToken(request.getToken()).getUser();

        if (StringUtils.isEmpty(request.getFrom()))
            throw new InsufficientDataProvidedException("fromCountry", "addNewUserCurrencyExchange");
        if (StringUtils.isEmpty(request.getTo()))
            throw new InsufficientDataProvidedException("toCountry", "addNewUserCurrencyExchange");
        CountryEntity from = countryService.findByName(request.getFrom());
        CountryEntity to = countryService.findByName(request.getTo());
        if (userCurrencyRepo.countAllByUserAndFromAndTo(user, from, to) == 0) {
            UserCurrencyEntity userCurrencyEntity = new UserCurrencyEntity();
            userCurrencyEntity.setUser(user);
            userCurrencyEntity.setFrom(from);
            userCurrencyEntity.setTo(to);
            return userCurrencyRepo.save(userCurrencyEntity);
        } else {
            return userCurrencyRepo.findByUserAndFromAndTo(user, from, to);
        }
    }
    @Transactional
    public void deleteUserCurrency(RemoveCurrencyRequestDto request) throws InsufficientDataProvidedException, CountryNotFoundException, SessionNotFoundException {
        UserEntity user = sessionService.findTokenEntityByToken(request.getToken()).getUser();

        if (StringUtils.isEmpty(request.getFrom()))
            throw new InsufficientDataProvidedException("fromCountry", "addNewUserCurrencyExchange");
        if (StringUtils.isEmpty(request.getTo()))
            throw new InsufficientDataProvidedException("toCountry", "addNewUserCurrencyExchange");
        CountryEntity from = countryService.findByName(request.getFrom());
        CountryEntity to = countryService.findByName(request.getTo());
        userCurrencyRepo.deleteByUserAndFromAndTo(user, from, to);
    }

    public List<UserCurrencyResponseDto> findUserCurrencyExchanges(String token) throws CountryNotFoundException, SessionNotFoundException {
        UserEntity user = sessionService.findTokenEntityByToken(token).getUser();
        List<UserCurrencyEntity> userCurrencyEntities = userCurrencyRepo.findAllByUser(user);
        List<UserCurrencyResponseDto> response = new ArrayList<>();
        for (UserCurrencyEntity userCurrencyEntity : userCurrencyEntities) {
            UserCurrencyResponseDto currency = new UserCurrencyResponseDto();
            currency.setFrom(userCurrencyEntity.getFrom().getName());
            currency.setTo(userCurrencyEntity.getTo().getName());
            currency.setRate(currencyService.convertCurrencyRate(userCurrencyEntity.getFrom().getName(), userCurrencyEntity.getTo().getName()));
            response.add(currency);
        }
        return response;
    }
}
