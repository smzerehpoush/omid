package com.mahdiyar.omid.server.model.dto.response;

import com.mahdiyar.omid.server.model.dto.AdminNotificationDto;
import com.mahdiyar.omid.server.model.entity.NotificationEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by mahdiyar on 11/19/18.
 */
@Data
@NoArgsConstructor
public class SendNotificationResponseDto extends AdminNotificationDto {
    public SendNotificationResponseDto(NotificationEntity notificationEntity) {
        super(notificationEntity);
    }
}
