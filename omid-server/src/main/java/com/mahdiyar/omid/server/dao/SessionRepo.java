package com.mahdiyar.omid.server.dao;

import com.mahdiyar.omid.server.model.entity.SessionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by mahdiyar on 6/9/18.
 */
@Repository
public interface SessionRepo extends JpaRepository<SessionEntity, String> {
    List<SessionEntity> findByDeviceIdAndExpirationDateNull(String deviceId);

    SessionEntity findByToken(String token);
}
