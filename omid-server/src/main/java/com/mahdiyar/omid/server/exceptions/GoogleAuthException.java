package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by mahdiyar on 6/6/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.GoogleAuthException, params = {"item"}, description = "google authentication exception")
public class GoogleAuthException extends ServiceException {
    private final String item;
    private final String operation;

    public GoogleAuthException(String item, String operation) {
        super("Insufficient data provided to do the operation. [item: " + item + ", operation: " + operation + "]");
        this.item = item;
        this.operation = operation;
    }

    public String getItem() {
        return item;
    }

    public String getOperation() {
        return operation;
    }
}
