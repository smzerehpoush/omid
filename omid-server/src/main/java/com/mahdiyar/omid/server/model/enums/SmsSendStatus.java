package com.mahdiyar.omid.server.model.enums;

/**
 * Created by mahdiyar on 6/6/18.
 */
public enum SmsSendStatus {
    SMS_Disabled,
    Failed,
    Successfully_Sent
}
