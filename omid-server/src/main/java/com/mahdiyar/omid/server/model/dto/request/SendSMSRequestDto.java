package com.mahdiyar.omid.server.model.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by mhd.zerehpoosh on 8/11/2019.
 */
@Data
@NoArgsConstructor
public class SendSMSRequestDto {
    private String mobileNo;
    private String serviceCode;
    private String content;
    private String subServiceCode;
    private String requestId;
    private String receivedId;
    private String hashtext;

}
