package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;
import lombok.Getter;

/**
 * Created by mahdiyar on 6/9/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.ActivationCodeMismatch, description = "When the activation code entered for sign up the device doesn't match the generated one")
@Getter
public class ActivationCodeMismatchException extends ServiceException {
    private final String mobileNo;
    private final String expected;
    private final String entered;

    public ActivationCodeMismatchException(String mobileNo, String expected, String entered) {
        super("expected Activation Code for mobileNo [" + mobileNo + "]: expected: [" + expected + "], entered: [" + entered + "]");
        this.mobileNo = mobileNo;
        this.expected = expected;
        this.entered = entered;
    }
}
