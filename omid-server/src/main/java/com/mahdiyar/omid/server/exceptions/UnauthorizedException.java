package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;
import com.mahdiyar.omid.server.model.enums.UserRole;

/**
 * Created by mahdiyar on 6/21/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.Unauthorized)
public class UnauthorizedException extends ServiceException {
    public UnauthorizedException(String token, UserRole[] roles) {
        super("Unauthorized Access");
    }
}
