package com.mahdiyar.omid.server.model;

import com.mahdiyar.omid.server.model.entity.SessionEntity;
import com.mahdiyar.omid.server.model.entity.UserEntity;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.UUID;

/**
 * Created by mahdiyar on 6/9/18.
 */
@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class RequestContext {
    private SessionEntity session;
    private String tokenStr;
    private Locale locale;
    private String requestId = UUID.randomUUID().toString();
    private HttpServletRequest request;
    private String clientIp;

    public SessionEntity getSession() {
        return session;
    }

    public void setSession(SessionEntity session) {
        this.session = session;
    }

    public UserEntity getUser() {
        return session != null ? session.getUser() : null;
    }

    public String getUserId() {
        UserEntity user = getUser();
        return user != null ? user.getId() : null;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public String getRequestId() {
        return requestId;
    }

    public String getTokenStr() {
        return tokenStr;
    }

    public void setTokenStr(String tokenStr) {
        this.tokenStr = tokenStr;
    }

    public String getUserMobileNo() {
        UserEntity user = this.getUser();
        return user != null ? user.getMobileNo() : null;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    public String getRequestRootUrl() {
        return this.request != null ? request.getRequestURL().toString().replace(request.getServletPath(), "") : null;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }
}
