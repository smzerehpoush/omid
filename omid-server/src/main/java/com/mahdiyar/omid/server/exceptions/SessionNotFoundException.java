package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by mahdiyar on 6/21/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.SessionNotFound)
public class SessionNotFoundException extends ServiceException {
    public SessionNotFoundException(String token) {
        super("Session not found");
    }
}
