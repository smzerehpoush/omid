package com.mahdiyar.omid.server.dao;

import com.mahdiyar.omid.server.model.entity.PictureEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PictureRepo extends JpaRepository<PictureEntity, String> {
}
