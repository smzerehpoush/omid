package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by mahdiyar on 10/28/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.CantFollowYourselfException)
public class CantFollowYourserlException extends ServiceException {
    public CantFollowYourserlException() {
        super("Cant Follow Yourself.");
    }
}
