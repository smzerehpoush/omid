package com.mahdiyar.omid.server.security;

import com.mahdiyar.omid.server.model.enums.UserRole;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by pc on 7/16/2017.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface AuthRequired {
    UserRole[] roles() default {UserRole.User, UserRole.Admin, UserRole.Tutor, UserRole.Agent};
}
