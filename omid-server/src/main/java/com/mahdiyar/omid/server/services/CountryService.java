package com.mahdiyar.omid.server.services;

import com.mahdiyar.omid.server.dao.CountryExchangeRateRepo;
import com.mahdiyar.omid.server.dao.CountryRepo;
import com.mahdiyar.omid.server.exceptions.*;
import com.mahdiyar.omid.server.model.entity.CountryEntity;
import com.mahdiyar.omid.server.model.entity.CountryExchangeRateEntity;
import com.mahdiyar.omid.server.utils.GeneralUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * Created by mahdiyar on 8/16/18.
 */
@Service
public class CountryService {
    private static final Logger logger = LoggerFactory.getLogger(CountryService.class);
    @Autowired
    private CountryRepo countryRepo;
    @Autowired
    private UserService userService;

    public CountryEntity findById(String id) throws CountryNotFoundException {
        CountryEntity countryEntity = countryRepo.findOne(id);
        if (countryEntity == null) {
            throw new CountryNotFoundException(id);
        }
        return countryEntity;
    }

    public CountryEntity findByName(String name) throws CountryNotFoundException {
        CountryEntity countryEntity = countryRepo.findByName(name);
        if (countryEntity == null) {
            throw new CountryNotFoundException(name);
        }
        return countryEntity;
    }

    public List<CountryEntity> findAll() {
        return countryRepo.findAll();
    }

    public CountryEntity addNewCountry(String name) throws InsufficientDataProvidedException {
        if (StringUtils.isEmpty(name))
            throw new InsufficientDataProvidedException("country-name", "addNewCountryInit");
        if (!countryRepo.existsByName(name)) {
            CountryEntity countryEntity = new CountryEntity();
            countryEntity.setName(name);
            return countryRepo.save(countryEntity);
        } else {
            return countryRepo.findByName(name);
        }

    }

    public CountryEntity addNewCountry(String name, String flagPhotoMediaId, String preCode, boolean smsActive) throws CountryAlreadyExistException, MediaNotFoundException, InsufficientDataProvidedException, InvalidDataException {

        if (StringUtils.isEmpty(name)) {
            throw new InsufficientDataProvidedException("name", "addNewCountry");
        }
        if (StringUtils.isEmpty(preCode)) {
            throw new InsufficientDataProvidedException("preCode", "addNewCountry");
        }
        if (!GeneralUtils.validatePreCode(preCode)) {
            throw new InvalidDataException("preCode", "addNewCountry");
        }
        String standardPreCode = GeneralUtils.getStandardPreCode(preCode, false);
        if (countryRepo.existsByName(name)) {
            throw new CountryAlreadyExistException(name);
        }
        if (countryRepo.existsByPreCode(standardPreCode)) {
            throw new CountryAlreadyExistException(standardPreCode);
        }
        CountryEntity countryEntity = new CountryEntity();
        countryEntity.setName(name);
        countryEntity.setPreCode(standardPreCode);
        countryEntity.setSmsActive(smsActive);
        countryRepo.save(countryEntity);
        return countryEntity;
    }

    public CountryEntity editCountry(String id, String name, String flagPhotoMediaId, String preCode, boolean smsActive) throws CountryNotFoundException, MediaNotFoundException {
        // TODO: 8/20/18 check duplicity of name and preCode
        // TODO: 8/20/18 validation
        CountryEntity countryEntity = findById(id);
        countryEntity.setName(name);
        countryEntity.setPreCode(GeneralUtils.getStandardPreCode(preCode, false));
        countryEntity.setSmsActive(smsActive);
        countryRepo.save(countryEntity);
        return countryEntity;
    }

    public void initCountries() {
        if (!countryRepo.checkExists("Iran", "98")) {
            CountryEntity countryEntity = new CountryEntity();
            countryEntity.setName("Iran");
            countryEntity.setPreCode("98");
            countryEntity.setSmsActive(true);
            countryRepo.save(countryEntity);
        }
    }

    public void deleteCountry(String countryId) throws CountryNotFoundException, DeleteNotPermittedException {
        CountryEntity countryEntity = findById(countryId);
        countryRepo.delete(countryEntity);
    }
}

