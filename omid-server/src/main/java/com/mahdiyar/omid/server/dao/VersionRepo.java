package com.mahdiyar.omid.server.dao;

import com.mahdiyar.omid.server.model.entity.VersionEntity;
import com.mahdiyar.omid.server.model.enums.Platform;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by mahdiyar on 11/7/18.
 */
@Repository
public interface VersionRepo extends JpaRepository<VersionEntity, String> {
    boolean existsByPlatformAndBuildNo(Platform platform, long buildNo);


    VersionEntity findByPlatformAndBuildNo(Platform platform, long buildNo);

    @Query("select v from VersionEntity v " +
            "where v.platform = :platform " +
            "and v.buildNo > :buildNo " +
            "and v.published = true")
    VersionEntity findLatestVersion(@Param("platform") Platform platform,
                                    @Param("buildNo") long buildNo);
}
