package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by mahdiyar on 6/22/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.FileUploadCountLimitReached)
public class FileUploadCountLimitReachedException extends ServiceException {
    public FileUploadCountLimitReachedException() {
        super("File upload count limit reached");
    }
}
