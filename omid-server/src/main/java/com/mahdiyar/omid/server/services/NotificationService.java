package com.mahdiyar.omid.server.services;

import com.currencyfair.onesignal.OneSignal;
import com.currencyfair.onesignal.model.notification.*;
import com.google.common.collect.ImmutableMap;
import com.mahdiyar.omid.server.dao.NotificationRepo;
import com.mahdiyar.omid.server.exceptions.CountryNotFoundException;
import com.mahdiyar.omid.server.exceptions.InsufficientDataProvidedException;
import com.mahdiyar.omid.server.model.entity.CountryEntity;
import com.mahdiyar.omid.server.model.entity.NotificationEntity;
import com.mahdiyar.omid.server.model.entity.UserEntity;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by mahdiyar on 11/23/18.
 */
@Service
@ConfigurationProperties("com.mahdiyar.omid.server.services.notification-service")
public class NotificationService {
    public static final String TAG_KEY_MOBILE_NUMBER = "mobile-number";
    public static final String TAG_KEY_EMAIL = "email";
    public static final String TAG_KEY_COUNTRY_ID = "country-id";


    @Setter
    private String onesignalAuthKey;
    @Setter
    private String onesignalAppId;
    @Setter
    private boolean pushEnabled;

    @Autowired
    private CountryService countryService;
    @Autowired
    private NotificationRepo notificationRepo;


    public Page<NotificationEntity> getUserNotifications(UserEntity user,
                                                         Long fromTimestamp,
                                                         Long toTimestamp,
                                                         int page,
                                                         int limit) {
        Date fromDate = fromTimestamp != null ? new Date(fromTimestamp) : null;
        Date toDate = toTimestamp != null ? new Date(toTimestamp) : null;
        return notificationRepo.findUserNotifications(
                user.getCountry() != null ? user.getCountry().getId() : null,
                fromDate,
                toDate,
                new PageRequest(page, limit));
    }

    public NotificationEntity sendNotification(String title,
                                               String subTitle,
                                               String body,
                                               String filterCountryId,
                                               UserEntity senderUser
    ) throws CountryNotFoundException, InsufficientDataProvidedException {
        if (StringUtils.isEmpty(title)) {
            throw new InsufficientDataProvidedException("title", "send-notification");
        }
        if (StringUtils.isEmpty(body)) {
            throw new InsufficientDataProvidedException("body", "send-notification");
        }
        CountryEntity filterCountry = !StringUtils.isEmpty(filterCountryId) ? countryService.findById(filterCountryId) : null;
        NotificationEntity notificationEntity = new NotificationEntity(title, subTitle, body, senderUser, filterCountry);
        notificationRepo.save(notificationEntity);

        if (pushEnabled) {
            sendOneSignalPushNotification(title, subTitle, filterCountryId, filterCountry, notificationEntity);
        }

        return notificationEntity;
    }

    private void sendOneSignalPushNotification(String title, String subTitle, String filterCountryId, CountryEntity filterCountry, NotificationEntity notificationEntity) {
        List<Filter> filters = new ArrayList<>();
        if (filterCountry != null) {
            filters.add(new Filter(Field.TAG, TAG_KEY_COUNTRY_ID, Relation.EQUALS, filterCountryId, Operator.AND));
        }
        Map<String, String> headings = ImmutableMap.of("en", title);
        Map<String, String> contents = ImmutableMap.of("en", subTitle);
        Map<String, String> data = ImmutableMap.of("notificationId", notificationEntity.getId());
        NotificationRequest notificationRequest = new NotificationRequest();
        notificationRequest.setAppId(onesignalAppId);
        notificationRequest.setData(data);
        notificationRequest.setFilters(filters);
        notificationRequest.setContents(contents);
        notificationRequest.setHeadings(headings);
        OneSignal.createNotification(onesignalAuthKey, notificationRequest);
    }

    public Page<NotificationEntity> searchAllNotifications(String countryId, Long fromTimestamp, Long toTimestamp, int page, int limit) {
        Date fromDate = fromTimestamp != null ? new Date(fromTimestamp) : null;
        Date toDate = toTimestamp != null ? new Date(toTimestamp) : null;
        return notificationRepo.searchNotifications(!StringUtils.isEmpty(countryId) ? countryId : null,
                fromDate,
                toDate,
                new PageRequest(page, limit)
        );

    }

    public void deleteById(String notificationId) {
        notificationRepo.delete(notificationId);
    }

    public void initializeNotifications() {

    }
}
