package com.mahdiyar.omid.server.dao;

import com.mahdiyar.omid.server.model.entity.CountryEntity;
import com.mahdiyar.omid.server.model.entity.CountryExchangeRateEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by mhd.zerehpoosh on 8/17/2019.
 */
@Repository
public interface CountryExchangeRateRepo extends JpaRepository<CountryExchangeRateEntity, String> {
    CountryExchangeRateEntity findFirstByCountryOrderByCreationDateDesc(CountryEntity country);
}
