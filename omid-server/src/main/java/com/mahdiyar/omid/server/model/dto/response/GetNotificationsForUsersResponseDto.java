package com.mahdiyar.omid.server.model.dto.response;

import com.mahdiyar.omid.server.model.dto.NotificationDto;
import com.mahdiyar.omid.server.model.entity.NotificationEntity;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mahdiyar on 11/19/18.
 */
@Data
@NoArgsConstructor
public class GetNotificationsForUsersResponseDto {
    private List<NotificationDto> data;
    private long filteredRecords;

    public GetNotificationsForUsersResponseDto(Page<NotificationEntity> notifications) {
        this.data = new ArrayList<>();
        for (NotificationEntity notificationEntity : notifications) {
            this.data.add(new NotificationDto(notificationEntity));
        }
        this.filteredRecords = notifications.getTotalElements();
    }
}
