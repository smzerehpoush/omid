package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by mahdiyar on 11/4/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.InstructorNotFound)
public class InstructorNotFoundException extends ServiceException {
    public InstructorNotFoundException() {
        super("instructor not found");
    }
}
