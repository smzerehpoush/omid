package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by mhd.zerehpoosh on 7/13/2019.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.EndDateMustBiggerThanStartDateException)
public class EndDateMustBiggerThanStartDateException extends ServiceException {
    public EndDateMustBiggerThanStartDateException() {
        super("end date must bigger than start date.");
    }
}
