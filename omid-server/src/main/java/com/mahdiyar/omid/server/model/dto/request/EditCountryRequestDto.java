package com.mahdiyar.omid.server.model.dto.request;

import lombok.Data;

/**
 * Created by mahdiyar on 8/16/18.
 */
@Data
public class EditCountryRequestDto {
    private String name;
    private String flagPhotoMediaId;
    private String preCode;
    private boolean smsActive;
}
