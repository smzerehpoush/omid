package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;
import lombok.Getter;

/**
 * Created by mahdiyar on 6/9/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.ExpiredActivationCode, params = {"mobileNo", "activationCodeId"}, description = "When the activation code is expired by exceeding the maximum try count or time window specified")
@Getter
public class ExpiredActivationCodeException extends ServiceException {
    private final String mobileNo;
    private final String activationCodeId;

    public ExpiredActivationCodeException(String mobileNo, String activationCodeId) {
        super("The activation code by id [" + activationCodeId + "] for mobileNo [" + mobileNo + "] is expired");
        this.mobileNo = mobileNo;
        this.activationCodeId = activationCodeId;
    }
}
