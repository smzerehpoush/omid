package com.mahdiyar.omid.server.model.dto;

import com.mahdiyar.omid.server.model.entity.VersionEntity;
import com.mahdiyar.omid.server.model.enums.Platform;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by mahdiyar on 11/7/18.
 */
@Data
@NoArgsConstructor
public class VersionDto {
    private String id;
    private Date creationDate;
    private Platform platform;
    private long buildNo;
    private String version;
    private String releaseNote;
    private boolean published;
    private boolean active;

    public VersionDto(VersionEntity versionEntity) {
        this.id = versionEntity.getId();
        this.creationDate = versionEntity.getCreationDate();
        this.platform = versionEntity.getPlatform();
        this.buildNo = versionEntity.getBuildNo();
        this.version = versionEntity.getVersion();
        this.releaseNote = versionEntity.getReleaseNote();
        this.published = versionEntity.isPublished();
        this.active = versionEntity.isActive();
    }
}
