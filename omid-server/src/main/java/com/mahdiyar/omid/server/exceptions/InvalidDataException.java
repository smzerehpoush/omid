package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by mahdiyar on 6/6/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.InvalidData, params = {"field", "operation"}, description = "Invalid data given for specified field on specified operation")
public class InvalidDataException extends ServiceException {
    private final String field;
    private final String operation;

    public InvalidDataException(String field, String operation) {
        super("Invalid data for field [" + field + "] on operation [" + operation + "]");
        this.field = field;
        this.operation = operation;
    }

    public String getField() {
        return field;
    }

    public String getOperation() {
        return operation;
    }
}
