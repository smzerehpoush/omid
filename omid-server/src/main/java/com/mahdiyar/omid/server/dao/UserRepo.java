package com.mahdiyar.omid.server.dao;

import com.mahdiyar.omid.server.model.entity.UserEntity;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by mahdiyar on 6/6/18.
 */
@Repository
public interface UserRepo extends JpaRepository<UserEntity, String> {

    UserEntity findByMobileNo(String mobileNo);

    UserEntity findDistinctByUsername(String username);

    UserEntity findByEmail(String email);

    boolean existsByUsername(String username);

    boolean existsByMobileNo(String mobileNo);

    boolean existsByEmail(String email);

    boolean existsByCountry_Id(String countryId);

    List<UserEntity> findByCountry_Id(String countryId, Sort sort);

    @Query("SELECT u FROM UserEntity u WHERE u.username like %:phrase% ORDER BY u.username desc")
    List<UserEntity> findAllByUsernameOrderByUsernameDesc(@Param("phrase") String phrase);

    UserEntity findByShareLink(String shareLink);

}
