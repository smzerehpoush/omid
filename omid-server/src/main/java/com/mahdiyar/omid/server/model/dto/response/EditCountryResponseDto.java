package com.mahdiyar.omid.server.model.dto.response;

import com.mahdiyar.omid.server.model.dto.CountryDto;
import com.mahdiyar.omid.server.model.entity.CountryEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by mahdiyar on 8/16/18.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class EditCountryResponseDto extends CountryDto {
    public EditCountryResponseDto(CountryEntity countryEntity) {
        super(countryEntity);
    }
}
