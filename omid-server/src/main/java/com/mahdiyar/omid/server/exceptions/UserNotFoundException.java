package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by mahdiyar on 6/6/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.UserNotFound, params = {"fieldName", "identifier"}, description = "The user by given identifier not found")
public class UserNotFoundException extends ServiceException {
    private final String identifier;
    private final String fieldName;

    private UserNotFoundException(String fieldName, String identifier) {
        super("User by " + fieldName + " [" + identifier + "] not found");
        this.fieldName = fieldName;
        this.identifier = identifier;
    }

    public static UserNotFoundException byId(String id) {
        return new UserNotFoundException("id", id);
    }

    public static UserNotFoundException byMobileNo(String mobileNo) {
        return new UserNotFoundException("mobileNo", mobileNo);
    }

    public static UserNotFoundException byEmail(String email) {
        return new UserNotFoundException("email", email);
    }

    public static UserNotFoundException byUsername(String username) {
        return new UserNotFoundException("username", username);
    }

    public String getIdentifier() {
        return this.identifier;
    }

    public String getFieldName() {
        return fieldName;
    }
}
