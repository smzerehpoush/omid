package com.mahdiyar.omid.server.model.dto.request;

import lombok.Data;

@Data
public class CreateSportRequestDto {
    private String name;
    private Integer order;
}
