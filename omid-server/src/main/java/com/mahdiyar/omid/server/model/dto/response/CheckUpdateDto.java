package com.mahdiyar.omid.server.model.dto.response;

import com.mahdiyar.omid.server.model.enums.CheckUpdateStatus;
import com.mahdiyar.omid.server.model.enums.Platform;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by mahdiyar on 11/7/18.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CheckUpdateDto {
    private CheckUpdateStatus status;
    private String newVersionReleaseNote;
    private Platform platform;
    private String newVersion;
    private long newVersionBuildNo;

}
