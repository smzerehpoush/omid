package com.mahdiyar.omid.server.model.dto.request;

import com.mahdiyar.omid.server.model.enums.Platform;
import lombok.Data;

/**
 * Created by mahdiyar on 9/4/18.
 */
@Data
public class LoginRequestDto {
    private String username;
    private String password;
    private Platform platform;
    private Long buildNo;
}
