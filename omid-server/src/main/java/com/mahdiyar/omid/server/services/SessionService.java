package com.mahdiyar.omid.server.services;

import com.mahdiyar.omid.server.dao.SessionRepo;
import com.mahdiyar.omid.server.exceptions.*;
import com.mahdiyar.omid.server.model.entity.SessionEntity;
import com.mahdiyar.omid.server.model.entity.UserEntity;
import com.mahdiyar.omid.server.model.entity.VersionEntity;
import com.mahdiyar.omid.server.model.enums.Platform;
import com.mahdiyar.omid.server.model.enums.UserRole;
import com.mahdiyar.omid.server.utils.GeneralUtils;
import com.mahdiyar.omid.server.utils.Pair;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by mahdiyar on 6/9/18.
 */
@Service
public class SessionService {
    private static final Logger logger = LoggerFactory.getLogger(SessionService.class);
    @Autowired
    private SessionRepo sessionRepo;
    @Autowired
    private VersionService versionService;


    public void expireActiveSessionsByDevice(String deviceId) throws UserNotFoundException {
        List<SessionEntity> activeSessions = findActiveSessionsByDevice(deviceId);
        for (SessionEntity sessionEntity : activeSessions) {
            expireSession(sessionEntity);
        }
    }

    List<SessionEntity> findActiveSessionsByDevice(String deviceId) {
        return sessionRepo.findByDeviceIdAndExpirationDateNull(deviceId);
    }

    public void expireSession(SessionEntity tokenEntity) {
        tokenEntity.setExpirationDate(new Date());
        sessionRepo.save(tokenEntity);
    }

    Pair<SessionEntity, String> createSessionEntityForUser(UserEntity userEntity, String deviceId, String deviceInfo, Platform platform, long buildNo, String ip) throws GenerateTokenException, VersionNotFoundException {
        VersionEntity version = versionService.findVersionByPlatformAndBuildNo(platform, buildNo);
        SessionEntity sessionEntity = new SessionEntity();
        sessionEntity.setDeviceId(deviceId);
        sessionEntity.setDeviceInfo(deviceInfo);
        sessionEntity.setPlatform(platform);
        sessionEntity.setUser(userEntity);
        sessionEntity.setIp(ip);
        sessionEntity.setVersion(version);
        String plainToken;
        try (InputStream inputStream = new ByteArrayInputStream(UUID.randomUUID().toString().getBytes())) {
            plainToken = DigestUtils.md5DigestAsHex(inputStream);
        } catch (IOException e) {
            logger.error("failed to generate token", e);
            throw new GenerateTokenException(e);
        }
        String hashedToken = GeneralUtils.getMd5HashBase64(plainToken);
        sessionEntity.setToken(hashedToken);
        sessionEntity.setExpirationDate(null);
        sessionRepo.save(sessionEntity);
        logger.debug("token created successfully [user: [mobileNo: " + userEntity.getMobileNo() + "]");
        return new Pair<>(sessionEntity, plainToken);
    }

    public SessionEntity validateToken(String token, UserRole[] roles, String ip) throws SessionNotFoundException, UnauthorizedException, ExpiredTokenException {
        logger.debug("trying to validate token [token: [plain token cannot be shown], roles: " + Arrays.toString(roles) + "]");
        if (StringUtils.isEmpty(token)) {
            logger.error("empty token");
            throw new SessionNotFoundException(token);
        }
        SessionEntity sessionEntity = findTokenEntityByToken(token);
        sessionEntity.setIp(ip);

        sessionRepo.save(sessionEntity);

        Date currentDate = new Date();
        if (sessionEntity.getExpirationDate() != null && currentDate.compareTo(sessionEntity.getExpirationDate()) > 0) {
            logger.error("token is expired");
            throw new ExpiredTokenException(token);
        }

        for (UserRole role : roles) {
            if (sessionEntity.getUser().getRole() == role) {
                logger.debug("token authenticated");
                return sessionEntity;
            }
        }

        logger.error("token not authorized");
        throw new UnauthorizedException(token, roles);
    }

    public SessionEntity findTokenEntityByToken(String token) throws SessionNotFoundException {
        SessionEntity sessionEntity = sessionRepo.findByToken(GeneralUtils.getMd5HashBase64(token));
        if (sessionEntity == null) {
            throw new SessionNotFoundException(token);
        }
        return sessionEntity;
    }
}
