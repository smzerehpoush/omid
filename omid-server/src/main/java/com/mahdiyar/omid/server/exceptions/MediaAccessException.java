package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

import java.io.IOException;

/**
 * Created by mahdiyar on 6/22/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.MediaAccess)
public class MediaAccessException extends ServiceException {
    public MediaAccessException(IOException e) {
        super("Cannot access media", e);
    }
}
