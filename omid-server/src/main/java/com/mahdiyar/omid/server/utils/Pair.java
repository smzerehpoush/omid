package com.mahdiyar.omid.server.utils;

import lombok.Getter;

/**
 * Created by mahdiyar on 6/21/18.
 */
@Getter
public class Pair<X, Y> {
    private X left;
    private Y right;

    public Pair(X left, Y right) {
        this.left = left;
        this.right = right;
    }
}
