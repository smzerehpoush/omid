package com.mahdiyar.omid.server.services;

import com.mahdiyar.omid.server.dao.VersionRepo;
import com.mahdiyar.omid.server.exceptions.InsufficientDataProvidedException;
import com.mahdiyar.omid.server.exceptions.MediaNotFoundException;
import com.mahdiyar.omid.server.exceptions.VersionAlreadyExistsException;
import com.mahdiyar.omid.server.exceptions.VersionNotFoundException;
import com.mahdiyar.omid.server.model.dto.response.CheckUpdateDto;
import com.mahdiyar.omid.server.model.entity.VersionEntity;
import com.mahdiyar.omid.server.model.enums.CheckUpdateStatus;
import com.mahdiyar.omid.server.model.enums.Platform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * Created by mahdiyar on 11/7/18.
 */
@Service
public class VersionService {
    private static final Logger logger = LoggerFactory.getLogger(VersionService.class);

    @Autowired
    private VersionRepo versionRepo;

    public List<VersionEntity> findAllVersions() {
        return versionRepo.findAll(new Sort(Sort.Direction.DESC, "creationDate"));
    }

    public VersionEntity createVersion(Platform platform, String version, long buildNo, boolean published, String releaseNote, boolean active) throws VersionNotFoundException, VersionAlreadyExistsException, MediaNotFoundException, InsufficientDataProvidedException {
        if (platform == null) {
            throw new InsufficientDataProvidedException("platform", "create-version");
        }
        if (StringUtils.isEmpty(buildNo)) {
            throw new InsufficientDataProvidedException("version", "create-version");
        }
        logger.info("trying to create version [platform: " + platform + ", version: " + version + ", buildNo: " + buildNo + ", published: " + published + "]");
        if (versionRepo.existsByPlatformAndBuildNo(platform, buildNo)) {
            throw new VersionAlreadyExistsException();
        }

        VersionEntity versionEntity = new VersionEntity();
        versionEntity.setPlatform(platform);
        versionEntity.setActive(active);
        versionEntity.setVersion(version);
        versionEntity.setBuildNo(buildNo);
        versionEntity.setPublished(published);
        versionEntity.setReleaseNote(releaseNote);
        versionEntity = versionRepo.save(versionEntity);

        return versionEntity;
    }

    public CheckUpdateDto checkUpdate(Platform platform, long buildNo) throws VersionNotFoundException {
        VersionEntity currentVersion = findVersionByPlatformAndBuildNo(platform, buildNo);
        VersionEntity latestVersion = versionRepo.findLatestVersion(platform, buildNo);
        CheckUpdateDto result = new CheckUpdateDto();
        result.setPlatform(platform);
        if (latestVersion != null) {
            result.setNewVersion(latestVersion.getVersion());
            result.setNewVersionBuildNo(latestVersion.getBuildNo());
            result.setNewVersionReleaseNote(latestVersion.getReleaseNote());
            result.setStatus(!currentVersion.isActive() ? CheckUpdateStatus.ForceUpdate : CheckUpdateStatus.UpdateAvailable);
        } else {
            result.setStatus(CheckUpdateStatus.NoUpdateAvailable);
        }
        return result;
    }

    public VersionEntity findVersionByPlatformAndBuildNo(Platform platform, long buildNo) throws VersionNotFoundException {
        VersionEntity versionEntity = versionRepo.findByPlatformAndBuildNo(platform, buildNo);
        if (versionEntity == null) {
            throw new VersionNotFoundException();
        }
        return versionEntity;
    }

    public VersionEntity findVersionEntityById(String id) throws VersionNotFoundException {
        VersionEntity versionEntity = versionRepo.findOne(id);
        if (versionEntity == null) {
            throw new VersionNotFoundException();
        }
        return versionEntity;
    }

    public VersionEntity editVersion(String versionId, Platform platform, String version, long buildNo, String releaseNote, boolean published, boolean active) throws VersionNotFoundException, InsufficientDataProvidedException, VersionAlreadyExistsException {
        assert !StringUtils.isEmpty(versionId);
        if (platform == null) {
            throw new InsufficientDataProvidedException("platform", "create-version");
        }
        if (StringUtils.isEmpty(buildNo)) {
            throw new InsufficientDataProvidedException("version", "create-version");
        }
        logger.info("trying to create version [platform: " + platform + ", version: " + version + ", buildNo: " + buildNo + ", published: " + published + "]");
        if (versionRepo.existsByPlatformAndBuildNo(platform, buildNo)) {
            throw new VersionAlreadyExistsException();
        }
        VersionEntity versionEntity = findVersionEntityById(versionId);

        versionEntity.setPlatform(platform);
        versionEntity.setActive(true);
        versionEntity.setVersion(version);
        versionEntity.setBuildNo(buildNo);
        versionEntity.setReleaseNote(releaseNote);
        versionEntity.setPublished(published);
        versionEntity.setActive(active);
        versionEntity = versionRepo.save(versionEntity);

        return versionEntity;
    }

    public void deleteById(String id) {
        versionRepo.delete(id);
    }

    public void initializeVersions() {
        if (!versionRepo.existsByPlatformAndBuildNo(Platform.Web, 1L)) {
            try {
                createVersion(Platform.Web, "1.0", 1, true, null, true);
            } catch (VersionNotFoundException | VersionAlreadyExistsException | MediaNotFoundException | InsufficientDataProvidedException e) {
                logger.error("failed to initialize version for Web (1)");
            }
        }

        if (!versionRepo.existsByPlatformAndBuildNo(Platform.Android, 1L)) {
            try {
                createVersion(Platform.Android, "1.0", 1, true, null, true);
            } catch (VersionNotFoundException | VersionAlreadyExistsException | MediaNotFoundException | InsufficientDataProvidedException e) {
                logger.error("failed to initialize version for Android (1)");
            }
        }

        if (!versionRepo.existsByPlatformAndBuildNo(Platform.IOS, 1L)) {
            try {
                createVersion(Platform.IOS, "1.0", 1, true, null, true);
            } catch (VersionNotFoundException | VersionAlreadyExistsException | MediaNotFoundException | InsufficientDataProvidedException e) {
                logger.error("failed to initialize version for IOS (1)");
            }
        }
    }

}
