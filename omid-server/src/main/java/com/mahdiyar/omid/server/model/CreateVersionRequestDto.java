package com.mahdiyar.omid.server.model;

import com.mahdiyar.omid.server.model.enums.Platform;
import lombok.Data;

/**
 * Created by mahdiyar on 11/7/18.
 */
@Data
public class CreateVersionRequestDto {
    private Platform platform;
    private long buildNo;
    private String version;
    private String releaseNote;
    private boolean published;
    private boolean active;
}
