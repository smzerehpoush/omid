package com.mahdiyar.omid.server.model.dto.response;

import com.mahdiyar.omid.server.model.dto.UserDto;
import com.mahdiyar.omid.server.model.entity.UserEntity;
import com.mahdiyar.omid.server.services.MessageService;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class GetAllUsersResponseDto {
    private List<UserDto> users;

    public GetAllUsersResponseDto(List<UserEntity> users, MessageService messageService) {
        this.users = new ArrayList<>();
        for (UserEntity userEntity : users) {
            this.users.add(new UserDto(userEntity, messageService));
        }
    }
}
