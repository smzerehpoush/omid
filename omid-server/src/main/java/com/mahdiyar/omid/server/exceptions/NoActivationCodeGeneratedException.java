package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;
import lombok.Getter;

/**
 * Created by mahdiyar on 6/9/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.NoActivationCodeGenerated, params = {"mobileNo", "activationCodeId"}, description = "When the given activationCodeId doesn't exist for the given mobile number")
@Getter
public class NoActivationCodeGeneratedException extends ServiceException {
    private final String mobileNo;
    private final String activationCodeId;

    public NoActivationCodeGeneratedException(String mobileNo, String activationCodeId) {
        super("no activation code by id [" + activationCodeId + "] generated for mobile number [" + mobileNo + "]");
        this.mobileNo = mobileNo;
        this.activationCodeId = activationCodeId;
    }
}
