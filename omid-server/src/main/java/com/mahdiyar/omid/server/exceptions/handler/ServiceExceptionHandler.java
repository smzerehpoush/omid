package com.mahdiyar.omid.server.exceptions.handler;

import com.mahdiyar.omid.server.exceptions.ServiceException;
import com.mahdiyar.omid.server.model.RestResponse;
import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;
import com.mahdiyar.omid.server.services.MessageService;
import com.mahdiyar.omid.server.utils.MessageUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mahdiyar on 7/6/18.
 */
@ControllerAdvice
public class ServiceExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(ServiceExceptionHandler.class);

    @Autowired
    private MessageService messageService;

    public static ExceptionDetails getExceptionDetails(Exception ex, MessageService messageService) {
        if (ex.getClass().isAnnotationPresent(HandledException.class)) {
            HandledException handledException = ex.getClass().getAnnotation(HandledException.class);
            int errorCode = handledException.enumerator().getErrorCode();

            String[] params = handledException.params();
            Map<String, Object> paramsMap = new HashMap<>();
            for (String param : params) {
                Object value;
                try {
                    value = ex.getClass().getMethod("get" + StringUtils.capitalize(param)).invoke(ex);
                } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                    logger.error("failed to fetch parameter [" + param + "] from exception: [class: " + ex.getClass() + "]", e);
                    value = null;
                }
                paramsMap.put(param, value);
            }
            String message = MessageUtils.putParams(messageService.getMessage("serviceException." + errorCode), paramsMap);
            if (ex instanceof ServiceException) {
                return new ExceptionDetails(errorCode, message, ((ServiceException) ex).getContents());
            } else {
                return new ExceptionDetails(errorCode, message, null);
            }
        } else {
            for (Throwable cause = ex.getCause(); cause != null; cause = cause.getCause()) {
                if (cause instanceof Exception && cause.getClass().isAnnotationPresent(HandledException.class)) {
                    return getExceptionDetails((Exception) cause, messageService);
                }
            }
            logger.error("internal server error", ex);
            return new ExceptionDetails(ServiceExceptionEnumerator.InternalServerError.getErrorCode(),
                    messageService.getMessage("serviceException." +
                            ServiceExceptionEnumerator.InternalServerError.getErrorCode()
                    ),
                    null
            );
        }
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<RestResponse> handleServiceException(Exception ex, WebRequest request) {
        ExceptionDetails details = getExceptionDetails(ex, messageService);
        if (details.getContent() != null) {
            return ResponseEntity.ok(RestResponse.error(details.code, details.getMessage(), details.getContent()));
        } else {
            return ResponseEntity.ok(RestResponse.error(details.getCode(), details.getMessage()));
        }
    }

    public static class ExceptionDetails {
        private int code;
        private String message;
        private Object content;

        private ExceptionDetails(int code, String message, Object content) {
            this.code = code;
            this.message = message;
            this.content = content;
        }


        public int getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

        public Object getContent() {
            return content;
        }
    }
}
