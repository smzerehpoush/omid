package com.mahdiyar.omid.server.model.enums;

/**
 * Created by mahdiyar on 6/6/18.
 */
public enum UserRole {
    User,
    Admin,
    Tutor,
    Agent
}
