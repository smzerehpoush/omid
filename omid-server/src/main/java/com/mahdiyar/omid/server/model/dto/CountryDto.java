package com.mahdiyar.omid.server.model.dto;

import com.mahdiyar.omid.server.model.entity.CountryEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by mahdiyar on 8/13/18.
 */
@Data
@NoArgsConstructor
public class CountryDto {
    private String id;
    private String name;
    private String preCode;
    private boolean smsActive;

    public CountryDto(CountryEntity country) {
        this.id = country.getId();
        this.name = country.getName();
        this.preCode = country.getPreCode();
        this.smsActive = country.isSmsActive();
    }
}
