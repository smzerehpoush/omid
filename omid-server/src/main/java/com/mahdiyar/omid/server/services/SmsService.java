package com.mahdiyar.omid.server.services;

import com.mahdiyar.omid.server.exceptions.SendSmsException;
import com.mahdiyar.omid.server.model.dto.request.SendSMSRequestDto;
import com.mahdiyar.omid.server.model.dto.response.SendSMSResponseDto;
import com.mahdiyar.omid.server.model.entity.UserEntity;
import com.mahdiyar.omid.server.model.enums.SmsSendStatus;
import com.mahdiyar.omid.server.model.enums.SmsType;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by mahdiyar on 6/6/18.
 */
@Service
@ConfigurationProperties("com.mahdiyar.omid.server.services.sms-service")
public class SmsService {
    private static final Logger logger = LoggerFactory.getLogger(SmsService.class);
    @Autowired
    private SmsLogService smsLogService;
    @Setter
    private String serviceCode;
    @Setter
    private String subServiceCode;
    @Setter
    private String requestId;
    @Setter
    private String receiveId;
    @Setter
    private String hashtext;
    @Setter
    private boolean sendSms;

    public void sendSms(String text, String logText, UserEntity user, SmsType smsType, String code) throws SendSmsException {
        assert user != null;
        assert user.getMobileNo() != null;
        String phoneNumber = user.getMobileNo();
        sendSms(text, logText, phoneNumber, smsType, code);
    }

    /*private void sendSMSKavenegar(String text, String logText, String phoneNumber, SmsType smsType) {
        KavenegarApi kavenegarApi= new KavenegarApi("776C574435396F6F466B584774743331486C3457686E4F744C52397073585652");
        kavenegarApi.send("100065995", )
    }*/

    public void sendSms(String text, String logText, String phoneNumber, SmsType smsType, String code) throws SendSmsException {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(() -> {
            SmsSendStatus status = null;
            String details = null;
            if (!sendSms) {
                logger.info("SMS send is deactivated");
                status = SmsSendStatus.SMS_Disabled;
                details = "SMS send is deactivated";
                return;
            }
            logger.info("trying to send sms to " + phoneNumber);
            try {
                sendSms(phoneNumber, text);

            } catch (Throwable e) {
                logger.error("failed to send sms ", e);
            } finally {
                smsLogService.saveNewLog(phoneNumber, logText, smsType, status, details);
            }
        });
    }

    public SendSMSResponseDto sendSms(String mobileNo, String smsText) {
        try {
            logger.info("SENDING SMS");

            SendSMSRequestDto requestSms = new SendSMSRequestDto();
            requestSms.setServiceCode(serviceCode);
            requestSms.setSubServiceCode(subServiceCode);
            requestSms.setMobileNo(mobileNo);
            requestSms.setContent(smsText);
            requestSms.setRequestId(requestId);
            requestSms.setReceivedId(receiveId);
            requestSms.setHashtext(hashtext);

            RestTemplate restTemplate = new RestTemplate();
            URI url = URI.create("http://10.30.138.20:9090/SmsSwitch/webresources/sms_service/send_sms");
            return restTemplate.postForObject(url, requestSms, SendSMSResponseDto.class);
        } catch (Exception e) {
            logger.error("cant send verification error");
            return null;
        }
    }

    public void initializeSms() {

    }
}
