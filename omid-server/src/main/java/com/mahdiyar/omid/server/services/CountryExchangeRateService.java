package com.mahdiyar.omid.server.services;

import com.mahdiyar.omid.server.dao.CountryExchangeRateRepo;
import com.mahdiyar.omid.server.exceptions.CountryNotFoundException;
import com.mahdiyar.omid.server.model.entity.CountryEntity;
import com.mahdiyar.omid.server.model.entity.CountryExchangeRateEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by mhd.zerehpoosh on 8/17/2019.
 */
@Service
public class CountryExchangeRateService {
    @Autowired
    private CountryExchangeRateRepo countryExchangeRateRepo;
    @Autowired
    private CountryService countryService;

    public CountryExchangeRateEntity addNewCountryExchangeRate(CountryEntity countryEntity, Double rate) {
        CountryExchangeRateEntity rateEntity = new CountryExchangeRateEntity();
        rateEntity.setCountry(countryEntity);
        rateEntity.setRate(rate);
        return countryExchangeRateRepo.save(rateEntity);
    }

    public Double getRate(String from) throws CountryNotFoundException {
        CountryEntity countryEntity = countryService.findByName(from);
        return countryExchangeRateRepo.findFirstByCountryOrderByCreationDateDesc(countryEntity).getRate();
    }
}
