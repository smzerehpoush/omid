package com.mahdiyar.omid.server.dao;

import com.mahdiyar.omid.server.model.entity.CountryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by mahdiyar on 8/16/18.
 */
@Repository
public interface CountryRepo extends JpaRepository<CountryEntity, String> {
    CountryEntity findByName(String name);

    boolean existsByName(String name);

    boolean existsByPreCode(String preCode);

    @Query("select count(c)>0 from CountryEntity c where c.name = :name or c.preCode = :preCode")
    boolean checkExists(@Param("name") String name,
                        @Param("preCode") String preCode);

    CountryEntity findByPreCode(String preCode);

    @Query("select count(c)>0 from CountryEntity c " +
            "where (c.name = :name and c.preCode = :preCode) and " +
            "c.id <> :countryId")
    boolean isDuplicateCountry(@Param("countryId") String countryId,
                               @Param("name") String name,
                               @Param("preCode") String preCode);
}
