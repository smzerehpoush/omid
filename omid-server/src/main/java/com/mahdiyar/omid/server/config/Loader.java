package com.mahdiyar.omid.server.config;

import com.mahdiyar.omid.server.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * Created by mahdiyar on 8/20/18.
 */
@Component
public class Loader implements ApplicationListener<ContextRefreshedEvent> {
    @Autowired
    private CountryService countryService;
    @Autowired
    private UserService userService;
    @Autowired
    private VersionService versionService;
    @Autowired
    private MessageService messageService;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private PictureService pictureService;
    @Autowired
    private SmsLogService smsLogService;
    @Autowired
    private SmsService smsService;
    @Autowired
    private CurrencyService currencyService;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        countryService.initCountries();
        countryService.initCountries();
        userService.initializeSysAdminUser();
        userService.initializeUsersShareLink();
        versionService.initializeVersions();
        messageService.initializeMessages();
        notificationService.initializeNotifications();
        pictureService.initializePictures();
        smsLogService.initializeSmsLogs();
        smsService.initializeSms();
        currencyService.initializeCurrencies();
    }
}
