package com.mahdiyar.omid.server.model.annotations;

import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by mahdiyar on 6/6/18.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface HandledException {
    ServiceExceptionEnumerator enumerator();

    String[] params() default {};

    String description() default "";
}
