package com.mahdiyar.omid.server.model.entity;

import com.mahdiyar.omid.server.model.enums.UserRole;
import lombok.Data;

import javax.persistence.*;
import java.util.Objects;
import java.util.UUID;

/**
 * Created by mahdiyar on 6/6/18.
 */
@Entity
@Table(name = "user")
@Data
public class UserEntity extends BaseEntity {
    /*@Column(name = "country", nullable = true)
    private CountryEntity country;*/
    @Column(name = "mobile_no", unique = true)
    private String mobileNo;
    @Column(name = "full_name")
    private String fullName;
    @Column(name = "username", /*nullable = false,*/ unique = true)
    private String username;
    @Column(name = "hashedPassword")
    private String hashedPassword;
    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private UserRole role;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "country_id")
    private CountryEntity country;
    @Column(name = "email", unique = true)
    private String email;
    @Column(name = "validated", nullable = false)
    private boolean validated;
    @Column(name = "active", nullable = false)
    private boolean active;
    @Column(name = "share_link", unique = true)
    private String shareLink;
    @Column(name = "bio", length = 100)
    private String bio;
    @Column(name = "verified")
    private Boolean verified;

    public UserEntity() {
        this.shareLink = UUID.randomUUID().toString().substring(0, 8);
    }

    public void setShareLink(String shareLink) {
        this.shareLink = shareLink;
    }

    /*@OneToOne
    @JoinColumn(name = "instructor_id")
    private InstructorEntity instructor;*/
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        UserEntity that = (UserEntity) o;
        return username.equals(that.username) &&
                id.equals(that.id);
    }

    @Override
    public String toString() {
        return "UserEntity{" +
                "mobileNo='" + mobileNo + '\'' +
                ", fullName='" + fullName + '\'' +
                ", username='" + username + '\'' +
                ", role=" + role +
                ", active=" + active +
                ", id='" + id + '\'' +
                ", creationDate=" + creationDate +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), username, id);
    }
}
