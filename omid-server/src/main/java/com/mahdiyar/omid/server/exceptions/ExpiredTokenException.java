package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by mahdiyar on 6/21/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.ExpiredToken)
public class ExpiredTokenException extends ServiceException {
    public ExpiredTokenException(String token) {
        super("Token has been expired");
    }
}
