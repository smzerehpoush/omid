package com.mahdiyar.omid.server.dao;

import com.mahdiyar.omid.server.model.entity.NotificationEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * Created by mahdiyar on 11/23/18.
 */
@Repository
public interface NotificationRepo extends JpaRepository<NotificationEntity, String> {

    @Query("select n " +
            "from NotificationEntity n " +
            "join n.filterCountry c " +
            "where (c.id is null or (c.id is not null and c.id = :userCountryId)) " +
            "and (:fromDate is null or (:fromDate is not null and  n.creationDate > :fromDate)) " +
            "and (:toDate is null or (:toDate is not null and n.creationDate < :toDate)) " +
            "order by n.creationDate desc")
    Page<NotificationEntity> findUserNotifications(@Param("userCountryId") String userCountryId,
                                                   @Param("fromDate") Date fromDate,
                                                   @Param("toDate") Date toDate,
                                                   Pageable pageable);

    @Query("select n " +
            "from NotificationEntity n " +
            "join n.filterCountry c " +
            "where (:countryId is null or (:countryId is not null and c.id = :countryId)) " +
            "and (:fromDate is null or (:fromDate is not null and n.creationDate > :fromDate)) " +
            "and (:toDate is null or (:toDate is not null and n.creationDate < :toDate)) " +
            "order by n.creationDate desc")
    Page<NotificationEntity> searchNotifications(@Param("countryId") String countryId,
                                                 @Param("fromDate") Date fromDate,
                                                 @Param("toDate") Date toDate,
                                                 Pageable pageable);
}
