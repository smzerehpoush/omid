package com.mahdiyar.omid.server.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by mahdiyar on 6/6/18.
 */
@Entity
@Table(name = "activation_code")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class ActivationCodeEntity extends BaseEntity {
    @Column(name = "code", length = 5, nullable = false)
    private String code;
    @ManyToOne(optional = false, cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)

    @JoinColumn(name = "user_id", nullable = false)
    private UserEntity user;
    @Column(name = "consumed", nullable = false)
    private boolean consumed;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "consume_date")
    private Date consumeDate;
    @Column(name = "tries_count")
    private Integer triesCount;
}
