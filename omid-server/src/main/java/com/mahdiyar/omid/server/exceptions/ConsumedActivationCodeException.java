package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by mahdiyar on 6/9/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.ConsumedActivationCode, description = "The activation code entered is consumed before")
public class ConsumedActivationCodeException extends ServiceException {
    private final String mobileNo;

    public ConsumedActivationCodeException(String mobileNo) {
        super("ActivationCode generated for mobile number [" + mobileNo + "] is already consumed");
        this.mobileNo = mobileNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }
}
