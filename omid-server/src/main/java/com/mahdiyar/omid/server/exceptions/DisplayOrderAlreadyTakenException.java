package com.mahdiyar.omid.server.exceptions;

/**
 * Created by mhd.zerehpoosh on 7/13/2019.
 */
public class DisplayOrderAlreadyTakenException extends ServiceException {
    public DisplayOrderAlreadyTakenException() {
        super("display order already taken.");
    }
}
