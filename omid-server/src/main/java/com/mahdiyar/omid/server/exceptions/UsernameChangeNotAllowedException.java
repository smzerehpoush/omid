package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by mahdiyar on 7/14/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.UsernameChangeNotAllowed)
public class UsernameChangeNotAllowedException extends ServiceException {
    public UsernameChangeNotAllowedException(String oldUsername, String newUsername) {
        super("username change not allowed from [" + oldUsername + "] to [" + newUsername + "]");
    }
}
