package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by mahdiyar on 6/22/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.MediaFileNotFound)
public class MediaFileNotFoundException extends ServiceException {
    public MediaFileNotFoundException(String id, String address) {
        super("Media file by id [" + id + "] and address [" + address + "] not found");
    }
}
