package com.mahdiyar.omid.server.model.dto;


import com.mahdiyar.omid.server.model.entity.PictureEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/***
 * created by mahdiyar
 */
@Data
@NoArgsConstructor
public class PictureDto {
    private String id;
    private Date creationDate;
    private String link;

    public PictureDto(PictureEntity pictureEntity) {
        this.id = pictureEntity.getId();
        this.creationDate = pictureEntity.getCreationDate();
        this.link = pictureEntity.getLink();
    }
}
