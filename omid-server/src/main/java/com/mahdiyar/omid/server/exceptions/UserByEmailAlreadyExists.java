package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by mahdiyar on 10/22/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.UserByEmailAlreadyExists)
public class UserByEmailAlreadyExists extends ServiceException {
    public UserByEmailAlreadyExists(String email) {
        super("user by email " + email + " already exists");
    }
}
