package com.mahdiyar.omid.server.model.dto.response;

import com.mahdiyar.omid.server.model.dto.UserDto;
import com.mahdiyar.omid.server.model.entity.UserEntity;
import com.mahdiyar.omid.server.services.MessageService;

/**
 * Created by mahdiyar on 6/21/18.
 */

public class UpdateProfileResponseDto extends UserDto {
    public UpdateProfileResponseDto(UserEntity userEntity, MessageService messageService) {
        super(userEntity, messageService);
    }
}
