package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by mahdiyar on 10/22/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.UserByMobileNumberAlreadyExists)
public class UserByMobileNoAlreadyExists extends Exception {
    public UserByMobileNoAlreadyExists(String plainMobileNo) {
        super("user by mobileNo " + plainMobileNo + " already exists");
    }
}
