package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;
import lombok.Getter;

/**
 * Created by mahdiyar on 6/9/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.FixerAPI, description = "when calling fixer.io API")
@Getter
public class FixerAPIException extends ServiceException {
}
