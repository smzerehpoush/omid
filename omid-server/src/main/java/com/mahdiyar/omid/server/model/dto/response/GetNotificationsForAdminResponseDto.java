package com.mahdiyar.omid.server.model.dto.response;

import com.mahdiyar.omid.server.model.dto.AdminNotificationDto;
import com.mahdiyar.omid.server.model.entity.NotificationEntity;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mahdiyar on 11/19/18.
 */
@Data
@NoArgsConstructor
public class GetNotificationsForAdminResponseDto {
    private List<AdminNotificationDto> notifications;
    private long filteredRecords;

    public GetNotificationsForAdminResponseDto(Page<NotificationEntity> notifications) {
        this.notifications = new ArrayList<>();
        notifications.forEach(notificationEntity -> this.notifications.add(new AdminNotificationDto(notificationEntity)));
        this.filteredRecords = notifications.getTotalElements();
    }
}
