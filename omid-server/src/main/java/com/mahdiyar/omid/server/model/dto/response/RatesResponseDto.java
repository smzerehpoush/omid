package com.mahdiyar.omid.server.model.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;

/**
 * Created by mhd.zerehpoosh on 8/17/2019.
 */
@Data
@NoArgsConstructor
public class RatesResponseDto {
    private String date;
    private boolean success;
    private HashMap<String, Double> rates = new HashMap<>();
    private Long timestamp;
    private String base;
}
