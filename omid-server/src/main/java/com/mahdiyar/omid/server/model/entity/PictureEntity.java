package com.mahdiyar.omid.server.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/***
 * created by mahdiyar
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@Entity
@Table(name = "picture")
public class PictureEntity extends BaseEntity {
    @Column(name = "link")
    private String link;
}
