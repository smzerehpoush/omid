package com.mahdiyar.omid.server.controller;

import com.mahdiyar.omid.server.exceptions.CountryNotFoundException;
import com.mahdiyar.omid.server.exceptions.FixerAPIException;
import com.mahdiyar.omid.server.exceptions.InsufficientDataProvidedException;
import com.mahdiyar.omid.server.model.RestResponse;
import com.mahdiyar.omid.server.services.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by mhd.zerehpoosh on 8/17/2019.
 */
@RestController
@RequestMapping(value = "/api/currency")
public class CurrencyController {
    @Autowired
    private CurrencyService currencyService;

    @GetMapping(value = "/refresh")
    public RestResponse<Void> initialize() throws InsufficientDataProvidedException, FixerAPIException {
        currencyService.initializeCurrencies();
        return RestResponse.ok();
    }

    @GetMapping(value = "/convert")
    public RestResponse<Double> covertRates(@RequestParam("from") String from, @RequestParam("to") String to) throws CountryNotFoundException {
        return RestResponse.ok(currencyService.convertCurrencyRate(from, to));
    }
}
