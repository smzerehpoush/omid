package com.mahdiyar.omid.server.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.mahdiyar.omid.server.dao.UserRepo;
import com.mahdiyar.omid.server.exceptions.*;
import com.mahdiyar.omid.server.model.entity.CountryEntity;
import com.mahdiyar.omid.server.model.entity.SessionEntity;
import com.mahdiyar.omid.server.model.entity.UserEntity;
import com.mahdiyar.omid.server.model.enums.Platform;
import com.mahdiyar.omid.server.model.enums.UserRole;
import com.mahdiyar.omid.server.utils.Pair;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
@ConfigurationProperties(prefix = "com.mahdiyar.omid.server.services.google-service")
public class GoogleService {
    private static final Logger logger = LoggerFactory.getLogger(UserService.class);
    private final String clientId = "1068853388359-7oorffis6urov9c02ifemdvptimpu1g9.apps.googleusercontent.com";
    GoogleIdTokenVerifier verifier;
    HttpTransport transport = new NetHttpTransport();
    JsonFactory jsonFactory = new JacksonFactory();
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private CountryService countryService;
    @Autowired
    private UserService userService;
    @Autowired
    private SessionService sessionService;

    @PostConstruct


    private void setGoogleVerifier() {
        verifier = new GoogleIdTokenVerifier.Builder(transport, jsonFactory)
                // Specify the CLIENT_ID of the app that accesses the backend:
                .setAudience(Collections.singletonList(clientId))
                // Or, if multiple clients access the backend:
                //.setAudience(Arrays.asList(CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3))
                .build();
    }

    public Pair<SessionEntity, String> googleSignupOrSignIn(String token, String countryId, String deviceId, String deviceInfo, Platform platform, Long buildNo, String ip) throws InsufficientDataProvidedException, GoogleAuthException, CountryNotFoundException {
        if (StringUtils.isEmpty(token)) {
            throw new InsufficientDataProvidedException("token", "google");
        }
        UserEntity userEntity = new UserEntity();
        try {
//            Map<String,String> googlemap = getMapFromGoogleTokenString(token);
//            boolean tok = doTokenVerification(googlemap);
            GoogleIdToken idToken = verifier.verify(token);


            if (idToken != null) {
                Payload payload = idToken.getPayload();
                String email = payload.getEmail();


                try {
                    userEntity = userService.findUserEntityByEmail(email);
                    logger.info("user by email [" + email + "] is already exists.");

                } catch (UserNotFoundException e) {
                    logger.info("creating user for email [" + email + "]...");
                    CountryEntity country = countryService.findById(countryId);

                    userEntity.setValidated(Boolean.valueOf(payload.getEmailVerified()));
                    userEntity.setUsername(email);
                    userEntity.setEmail(email);
                    userEntity.setCountry(country);
                    userEntity.setRole(UserRole.User);
                    userEntity.setFullName((String) payload.get("name"));
                    userEntity.setValidated(true);
                    userRepo.save(userEntity);
                    logger.info("user created for email [" + email + "]: id: [" + userEntity.getId() + "]");
                }

            } else {
                System.out.println("Invalid ID token.");
            }

            sessionService.expireActiveSessionsByDevice(deviceId);
            Pair<SessionEntity, String> result = sessionService.createSessionEntityForUser(userEntity, deviceId, deviceInfo, platform, buildNo, ip);
            logger.info("user [userId: " + userEntity.getId() + "] successfully activated.");
            return result;


        } catch (GeneralSecurityException | IOException | UserNotFoundException | GenerateTokenException | VersionNotFoundException e) {
            throw new GoogleAuthException(e.getMessage(), "");
        }

    }

    private Map<String, String> getMapFromGoogleTokenString(final String idTokenString) {
        BufferedReader in = null;
        try {
            // get information from token by contacting the google_token_verify_tool url :
            in = new BufferedReader(new InputStreamReader(
                    (new URL("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=" + idTokenString.trim()))
                            .openConnection().getInputStream(), Charset.forName("UTF-8")));

            // read information into a string buffer :
            StringBuffer b = new StringBuffer();
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                b.append(inputLine + "\n");
            }

            // transforming json string into Map<String,String> :
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(b.toString(), objectMapper.getTypeFactory().constructMapType(Map.class, String.class, String.class));

            // exception handling :
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println("\n\n\tFailed to transform json to string\n");
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    // chack the "email_verified" and "email" values in token payload
    private boolean verifyEmail(final Map<String, String> tokenPayload) {
        if (tokenPayload.get("email_verified") != null && tokenPayload.get("email") != null) {
            try {
                return Boolean.valueOf(tokenPayload.get("email_verified")) && tokenPayload.get("email").contains("@gmail.");
            } catch (Exception e) {
                System.out.println("\n\n\tCheck emailVerified failed - cannot parse " + tokenPayload.get("email_verified") + " to boolean\n");
            }
        } else {
            System.out.println("\n\n\tCheck emailVerified failed - required information missing in the token");
        }
        return false;
    }

    // check token expiration is after now :
    private boolean checkExpirationTime(final Map<String, String> tokenPayload) {
        try {
            if (tokenPayload.get("exp") != null) {
                // the "exp" value is in seconds and Date().getTime is in mili seconds
                return Long.parseLong(tokenPayload.get("exp") + "000") > new java.util.Date().getTime();
            } else {
                System.out.println("\n\n\tCheck expiration failed - required information missing in the token\n");
            }
        } catch (Exception e) {
            System.out.println("\n\n\tCheck expiration failed - cannot parse " + tokenPayload.get("exp") + " into long\n");
        }
        return false;
    }

    // check that at least one CLIENT_ID matches with token values
    private boolean checkAudience(final Map<String, String> tokenPayload) {
        if (tokenPayload.get("aud") != null && tokenPayload.get("azp") != null) {
            List<String> pom = Arrays.asList(clientId,
                    "MY_CLIENT_ID_2",
                    "MY_CLIENT_ID_3");

            if (pom.contains(tokenPayload.get("aud")) || pom.contains(tokenPayload.get("azp"))) {
                return true;
            } else {
                System.out.println("\n\n\tCheck audience failed - audiences differ\n");
                return false;
            }
        }
        System.out.println("\n\n\tCheck audience failed - required information missing in the token\n");
        return false;
    }

    // verify google token payload :
    private boolean doTokenVerification(final Map<String, String> tokenPayload) {
        if (tokenPayload != null) {
            return verifyEmail(tokenPayload) // check that email address is verifies
                    && checkExpirationTime(tokenPayload) // check that token is not expired
                    && checkAudience(tokenPayload) // check audience
                    ;
        }
        return false;
    }

}
