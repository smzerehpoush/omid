package com.mahdiyar.omid.server.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by mhd.zerehpoosh on 8/17/2019.
 */
@Entity
@Table(name = "country_exchange_rate")
@Data
@NoArgsConstructor
public class CountryExchangeRateEntity extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "country_id")
    private CountryEntity country;
    @Column(name = "rate")
    private Double rate;
}
