package com.mahdiyar.omid.server.model.dto.response;

import com.mahdiyar.omid.server.model.dto.UserDto;
import com.mahdiyar.omid.server.model.entity.SessionEntity;
import com.mahdiyar.omid.server.services.MessageService;
import lombok.Data;

import java.util.Date;

/**
 * Created by mahdiyar on 9/4/18.
 */
@Data
public class LoginResponseDto {
    private String token;
    private UserDto userInfo;
    private Date expirationDate;

    public LoginResponseDto(SessionEntity sessionEntity, String token, MessageService messageService) {
        this.token = token;
        this.userInfo = new UserDto(sessionEntity.getUser(), messageService);
        this.expirationDate = sessionEntity.getExpirationDate();
    }
}
