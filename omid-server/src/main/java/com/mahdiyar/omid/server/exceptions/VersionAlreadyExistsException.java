package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by mahdiyar on 11/7/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.VersionAlreadyExists)
public class VersionAlreadyExistsException extends ServiceException {
    public VersionAlreadyExistsException() {

    }
}
