package com.mahdiyar.omid.server.controller;

import com.mahdiyar.omid.server.exceptions.InsufficientDataProvidedException;
import com.mahdiyar.omid.server.exceptions.MediaNotFoundException;
import com.mahdiyar.omid.server.exceptions.VersionAlreadyExistsException;
import com.mahdiyar.omid.server.exceptions.VersionNotFoundException;
import com.mahdiyar.omid.server.model.CreateVersionRequestDto;
import com.mahdiyar.omid.server.model.RestResponse;
import com.mahdiyar.omid.server.model.dto.request.EditVersionRequestDto;
import com.mahdiyar.omid.server.model.dto.response.*;
import com.mahdiyar.omid.server.model.entity.VersionEntity;
import com.mahdiyar.omid.server.model.enums.Platform;
import com.mahdiyar.omid.server.model.enums.UserRole;
import com.mahdiyar.omid.server.security.AuthRequired;
import com.mahdiyar.omid.server.services.VersionService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by mahdiyar on 11/7/18.
 */
@RestController
@RequestMapping("/api/version")
public class VersionController {
    @Autowired
    private VersionService versionService;

    @ApiOperation("Checks if update available")
    @GetMapping("/checkUpdate")
    public RestResponse<CheckUpdateDto> checkUpdate(@RequestParam("platform") Platform platform,
                                                    @RequestParam("buildNo") long buildNo) throws VersionNotFoundException {
        CheckUpdateDto checkUpdateDto = versionService.checkUpdate(platform, buildNo);
        return RestResponse.ok(checkUpdateDto);

    }

    @ApiOperation("Create new Version")
    @AuthRequired(roles = UserRole.Admin)
    @PostMapping
    public RestResponse<CreateVersionResponseDto> createNewVersion(@RequestBody CreateVersionRequestDto request) throws MediaNotFoundException, VersionNotFoundException, InsufficientDataProvidedException, VersionAlreadyExistsException {
        VersionEntity versionEntity = versionService.createVersion(request.getPlatform(),
                request.getVersion(),
                request.getBuildNo(),
                request.isPublished(),
                request.getReleaseNote(),
                request.isActive());
        return RestResponse.ok(new CreateVersionResponseDto(versionEntity));
    }

    @ApiOperation(value = "Get all Versions", notes = "Admin Auth Required")
    @AuthRequired(roles = UserRole.Admin)
    @GetMapping
    public RestResponse<GetAllVersionsResponseDto> getAllVersions() {
        List<VersionEntity> versions = versionService.findAllVersions();
        return RestResponse.ok(new GetAllVersionsResponseDto(versions));
    }

    @ApiOperation(value = "Get a Version by ID", notes = "Admin Auth Required")
    @AuthRequired(roles = UserRole.Admin)
    @GetMapping("/{versionId}")
    public RestResponse<GetVersionResponseDto> getVersion(@PathVariable("versionId") String versionId) throws VersionNotFoundException {
        VersionEntity versionEntity = versionService.findVersionEntityById(versionId);
        return RestResponse.ok(new GetVersionResponseDto(versionEntity));
    }

    @ApiOperation(value = "Edit a Version", notes = "Admin Auth Required")
    @AuthRequired(roles = UserRole.Admin)
    @PostMapping("/{versionId}")
    public RestResponse<EditVersionResponseDto> editVersion(@PathVariable("versionId") String versionId,
                                                            @RequestBody EditVersionRequestDto request) throws VersionAlreadyExistsException, InsufficientDataProvidedException, VersionNotFoundException {
        VersionEntity versionEntity = versionService.editVersion(versionId,
                request.getPlatform(),
                request.getVersion(),
                request.getBuildNo(),
                request.getReleaseNote(),
                request.isPublished(),
                request.isActive());
        return RestResponse.ok(new EditVersionResponseDto(versionEntity));
    }

    @ApiOperation(value = "Delete Version", notes = "Admin Auth Required")
    @AuthRequired(roles = UserRole.Admin)
    @DeleteMapping("/{versionId}")
    public RestResponse<Void> deleteVersion(@PathVariable("versionId") String versionId) {
        versionService.deleteById(versionId);
        return RestResponse.ok();
    }

}
