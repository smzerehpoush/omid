package com.mahdiyar.omid.server.model.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by mhd.zerehpoosh on 8/18/2019.
 */
@Data
@NoArgsConstructor
public class CountryResponseDto {
    private String id;
    private String name;

    public CountryResponseDto(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
