package com.mahdiyar.omid.server.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.util.Locale;

/**
 * Created by mahdiyar on 6/29/18.
 */
@Configuration
public class LocaleConfig {
    @Value("${default-locale.language}")
    private String defaultLang;
    @Value("${default-locale.country}")
    private String defaultCountry;

    @Bean
    public Locale defaultLocale() {
        return new Locale(defaultLang, defaultCountry);
    }

    @Bean
    public LocaleResolver localeResolver(Locale defaultLocale) {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(defaultLocale);
        return slr;
    }

    @Bean
    public ResourceBundleMessageSource messageSource() {
        ResourceBundleMessageSource source = new ResourceBundleMessageSource();
        source.setBasenames("i18n/messages");  // name of the resource bundle
        source.setUseCodeAsDefaultMessage(true);
        return source;
    }
}
