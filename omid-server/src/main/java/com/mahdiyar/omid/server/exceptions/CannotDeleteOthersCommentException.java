package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * @author Seyyed Mahdiyar Zerehpoush
 */
@HandledException(enumerator = ServiceExceptionEnumerator.CannotDeleteOthersComment)

public class CannotDeleteOthersCommentException extends ServiceException {
}
