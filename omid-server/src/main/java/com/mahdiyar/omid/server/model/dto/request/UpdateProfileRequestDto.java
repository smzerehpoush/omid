package com.mahdiyar.omid.server.model.dto.request;

import lombok.Data;

/**
 * Created by mahdiyar on 6/21/18.
 */
@Data
public class UpdateProfileRequestDto {
    private String username;
    private String fullName;
    private String profilePictureMediaId;
    private String bio;
}
