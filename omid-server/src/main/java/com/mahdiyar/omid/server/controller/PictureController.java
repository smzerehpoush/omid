package com.mahdiyar.omid.server.controller;

import com.mahdiyar.omid.server.model.RequestContext;
import com.mahdiyar.omid.server.services.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/***
 * created by mahdiyar
 */
@RestController
@RequestMapping("/api/picture")
public class PictureController {
    @Autowired
    private PictureService pictureService;
    @Autowired
    private RequestContext requestContext;
}
