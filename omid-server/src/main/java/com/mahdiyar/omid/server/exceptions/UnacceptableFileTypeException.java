package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by mahdiyar on 6/22/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.UnacceptableFileType, params = {"type"})
public class UnacceptableFileTypeException extends ServiceException {
    private final String type;

    public UnacceptableFileTypeException(String type) {
        super("Unaccpetable file type [" + type + "] to upload");
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
