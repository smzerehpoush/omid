package com.mahdiyar.omid.server.model.dto.response;

import com.mahdiyar.omid.server.model.dto.UserDto;
import com.mahdiyar.omid.server.model.entity.ActivationCodeEntity;
import com.mahdiyar.omid.server.model.entity.UserEntity;
import com.mahdiyar.omid.server.services.MessageService;
import lombok.Data;

/**
 * Created by mahdiyar on 6/21/18.
 */
@Data
public class UserSignUpResponseDto extends UserDto {
    private String mobileNo;

    public UserSignUpResponseDto(ActivationCodeEntity activationCodeEntity, MessageService messageService) {
        this(activationCodeEntity.getUser(), messageService);

    }

    public UserSignUpResponseDto(UserEntity userEntity, MessageService messageService) {
        super(userEntity, messageService);
        this.mobileNo = userEntity.getMobileNo();
    }
}
