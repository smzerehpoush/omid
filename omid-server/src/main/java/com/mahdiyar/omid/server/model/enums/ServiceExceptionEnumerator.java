package com.mahdiyar.omid.server.model.enums;


/**
 * Created by mahdiyar on 6/6/18.
 */
public enum ServiceExceptionEnumerator {
    InternalServerError(99),
    InsufficientDataProvided(100),
    InvalidData(101),
    DeleteNotPermitted(102),
    GenerateToken(103),
    SessionNotFound(104),
    ExpiredToken(105),
    Unauthorized(106),
    FixerAPI(107),
    UsernameAlreadyTakenException(108),
    UserNotFound(110),
    InvalidUsername(111),
    UsernameAlreadyTaken(112),
    UsernameChangeNotAllowed(113),
    WrongPassword(114),
    UserByMobileNumberAlreadyExists(115),
    UserByEmailAlreadyExists(116),
    UserNotValidated(117),
    CampaignNotFound(118),
    PostNotFound(119),
    NoActivationCodeGenerated(120),
    ConsumedActivationCode(121),
    ExpiredActivationCode(122),
    ActivationCodeMismatch(123),
    CantFollowYourselfException(124),
    EndDateMustBiggerThanStartDateException(125),
    CannotDeleteOthersComment(126),

    //media
    FileUploadCountLimitReached(130),
    FileSizeLimitExceeded(131),
    UnacceptableFileType(132),
    MediaUpload(133),
    MediaNotFound(134),
    MediaFileNotFound(135),
    MediaAccess(136),
    UserMediaAccessRestricted(137),
    CanNotCreateResortComment(138),


    SendSmsFailed(200),

    //resort
    ResortNotFound(210),
    ResortFeatureNotFound(211),
    ResortAlreadyExists(212),

    //country
    CountryNotFound(220),
    CountryAlreadyExists(221),

    //learning video
    LearningVideoNotFound(230),

    //comment
    CommentNotFound(240),
    LearningVideoCommentNotFound(241),

    //instructor
    InstructorNotFound(250),

    //version
    VersionAlreadyExists(260),
    VersionNotFound(261),

    //sport
    SportNotFoundException(270),

    //    google auth
    GoogleAuthException(280);


    private int errorCode;

    ServiceExceptionEnumerator(int errorCode) {
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }

}
