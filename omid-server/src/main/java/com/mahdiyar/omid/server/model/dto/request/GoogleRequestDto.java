package com.mahdiyar.omid.server.model.dto.request;

import com.mahdiyar.omid.server.model.enums.Platform;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by mahdiyar on 6/21/18.
 */
@Data
@NoArgsConstructor
public class GoogleRequestDto {
    private String token;
    private String countryId;
    private String deviceId;
    private String deviceInfo;
    private Platform platform;
    private Long buildNo;
}
