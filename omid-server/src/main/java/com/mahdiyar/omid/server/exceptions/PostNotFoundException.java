package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

@HandledException(enumerator = ServiceExceptionEnumerator.PostNotFound)
public class PostNotFoundException extends ServiceException {
    public PostNotFoundException() {
        super("post not found.");
    }
}
