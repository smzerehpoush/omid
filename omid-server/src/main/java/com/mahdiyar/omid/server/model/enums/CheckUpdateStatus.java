package com.mahdiyar.omid.server.model.enums;

/**
 * Created by mahdiyar on 11/7/18.
 */
public enum CheckUpdateStatus {
    NoUpdateAvailable,
    UpdateAvailable,
    ForceUpdate
}
