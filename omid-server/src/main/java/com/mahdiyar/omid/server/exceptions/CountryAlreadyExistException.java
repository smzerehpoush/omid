package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by mahdiyar on 8/16/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.CountryAlreadyExists)
public class CountryAlreadyExistException extends ServiceException {
    public CountryAlreadyExistException(String name) {
        super("Country by name [" + name + "] already exists");
    }
}
