package com.mahdiyar.omid.server.model.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Seyyed Mahdiyar Zerehpoush
 */
@Data
@NoArgsConstructor
public class UserCurrencyResponseDto {
    private String from;
    private String to;
    private Double rate;
}
