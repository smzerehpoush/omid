package com.mahdiyar.omid.server.model.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by mahdiyar on 11/19/18.
 */
@Data
@NoArgsConstructor
public class SendPushNotificationRequestDto {
    private String title;
    private String subTitle;
    private String body;
    private String countryId;
}
