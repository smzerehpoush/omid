package com.mahdiyar.omid.server.model.dto.response;

import com.mahdiyar.omid.server.model.entity.CountryEntity;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mahdiyar on 8/16/18.
 */
@Data
public class GetAllCountryNamesResponseDto {
    private List<String> countryNames;

    public GetAllCountryNamesResponseDto(List<CountryEntity> countries) {
        this.countryNames = new ArrayList<>();
        for (CountryEntity country : countries) {
            this.countryNames.add(country.getName());
        }
    }
}
