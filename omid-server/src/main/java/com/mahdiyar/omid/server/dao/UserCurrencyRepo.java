package com.mahdiyar.omid.server.dao;

import com.mahdiyar.omid.server.model.entity.CountryEntity;
import com.mahdiyar.omid.server.model.entity.UserCurrencyEntity;
import com.mahdiyar.omid.server.model.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Seyyed Mahdiyar Zerehpoush
 */
@Repository
public interface UserCurrencyRepo extends JpaRepository<UserCurrencyEntity, String> {
    Integer countAllByUserAndFromAndTo(UserEntity user, CountryEntity from, CountryEntity to);

    UserCurrencyEntity findByUserAndFromAndTo(UserEntity user, CountryEntity from, CountryEntity to);

    List<UserCurrencyEntity> findAllByUser(UserEntity user);
    Long deleteByUserAndFromAndTo(UserEntity user, CountryEntity from, CountryEntity to);
}
