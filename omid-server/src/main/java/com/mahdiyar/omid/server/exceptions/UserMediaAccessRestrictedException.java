package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by mahdiyar on 6/22/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.UserMediaAccessRestricted)
public class UserMediaAccessRestrictedException extends ServiceException {
    public UserMediaAccessRestrictedException(String userId, String mediaId) {
        super("Media access restricted to userId [" + userId + "] and mediaId [" + mediaId + "]");
    }
}
