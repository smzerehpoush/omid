package com.mahdiyar.omid.server.model.enums;

/**
 * Created by mahdiyar on 6/9/18.
 */
public enum Platform {
    Android,
    IOS,
    Web

}
