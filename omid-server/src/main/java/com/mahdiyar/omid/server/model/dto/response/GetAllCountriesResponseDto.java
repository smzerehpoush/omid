package com.mahdiyar.omid.server.model.dto.response;

import com.mahdiyar.omid.server.model.dto.CountryDto;
import com.mahdiyar.omid.server.model.entity.CountryEntity;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mahdiyar on 8/16/18.
 */
@Data
public class GetAllCountriesResponseDto {
    private List<CountryDto> countries;

    public GetAllCountriesResponseDto(List<CountryEntity> countries) {
        this.countries = new ArrayList<>();
        for (CountryEntity country : countries) {
            this.countries.add(new CountryDto(country));
        }
    }
}
