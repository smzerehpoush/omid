package com.mahdiyar.omid.server.services;

import com.mahdiyar.omid.server.model.RequestContext;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.Locale;
import java.util.Map;

/**
 * Created by mahdiyar on 6/9/18.
 */
@Service
public class MessageService {
    private static Locale farsiLocale = new Locale("fa", "IR");
    private static Locale englishLocale = new Locale("en", "US");
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private RequestContext requestContext;
    @Autowired
    private Locale defaultLocale;

    public String getMessage(String key) {
        Locale locale = getLocale();

        return getMessage(key, locale);
    }

    private Locale getLocale() {
        Locale locale = null;
        try {
            locale = requestContext.getLocale();
        } catch (BeanCreationException e) {
            //ignored
        }
        if (locale == null) {
            locale = defaultLocale;
        }
        return locale;
    }

    public String getEnMessage(String key) {
        return getMessage(key, englishLocale);
    }

    public String getMessage(String key, Map<String, Object> params) {
        String message = getMessage(key);
        for (Map.Entry<String, Object> paramEntry : params.entrySet()) {
            message = message.replace("{" + paramEntry.getKey() + "}", String.valueOf(paramEntry.getValue()));
        }
        return message;
    }

    /*public String getMessageWithLocale(String key, Locale locale, Object... params) {
        return getMessage(key, locale, params);
    }*/

    /*public String getMessage(String key, Object... params) {
        Locale locale = null;
        try {
            locale = requestContext.getLocale();
        } catch (BeanCreationException e) {
            //ignored
        }
        if (locale == null) {
            locale = defaultLocale;
        }
        return getMessage(key, locale, params);
    }*/

//    private String getMessage(String key, Locale locale) {
//        return getMessage(key, locale, new Object[0]);
//    }

    private String getMessage(String key, Locale locale) {
        return messageSource.getMessage(key, new Object[0], locale);
    }

    public void initializeMessages() {

    }
}