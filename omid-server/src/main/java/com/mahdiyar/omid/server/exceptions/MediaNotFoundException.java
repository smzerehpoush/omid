package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by mahdiyar on 6/22/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.MediaNotFound)
public class MediaNotFoundException extends ServiceException {
    public MediaNotFoundException(String mediaId) {
        super("Media by id [" + mediaId + "] not found");
    }
}
