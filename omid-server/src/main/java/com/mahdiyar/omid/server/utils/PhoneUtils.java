package com.mahdiyar.omid.server.utils;

import org.apache.commons.lang.StringUtils;

/**
 * Created by mahdiyar on 6/6/18.
 */
public class PhoneUtils {
    public static String toPlain(String countryCode, String phone) {
        assert !StringUtils.isEmpty(phone);
        String formattedPhoneNo = GeneralUtils.convertPersianDigitsToStandard(phone).replace("\\D", "");
        /*if (formattedPhoneNo.startsWith("98")) {
            formattedPhoneNo = "0" + formattedPhoneNo.substring(2);
        } else if (formattedPhoneNo.startsWith("9")) {
            formattedPhoneNo = "0" + formattedPhoneNo;
        }
        formattedPhoneNo = GeneralUtils.convertPersianDigitsToStandard(formattedPhoneNo)
                .replace("+98", "0")
                .replace("+", "00")
                .replaceAll("[^0-9]+", "");*/
        if (formattedPhoneNo.startsWith("0"))
            formattedPhoneNo = formattedPhoneNo.substring(1);
        return countryCode + formattedPhoneNo;
    }

    public static String getCountryCodeIncludedMobileNo(String mobileNo) {

        if (mobileNo.startsWith("00")) {
            return mobileNo.substring(2);
        } else if (mobileNo.startsWith("0")) {
            return "98" + mobileNo.substring(1);
        } else {
            return mobileNo;
        }
    }

    public static boolean validateMobileNo(String countryPreCode, String mobileNo) {
        String plainNumber = toPlain(countryPreCode, mobileNo);
        if (StringUtils.isEmpty(plainNumber)) {
            return false;
        }
        /*if (plainNumber.length() > 11) {
            return false;
        }*/
        /*if (plainNumber.length() == 0) {
            return false;
        }*/
        /*if (plainNumber.startsWith("00") || plainNumber.startsWith("0")) {
            if (plainNumber.replaceAll("\\D", "").length() == plainNumber.length()) {
                return true;
            }
        }*/
        if (plainNumber.replaceAll("\\D", "").length() == plainNumber.length()) {
            return true;
        }
        return true;
    }
}
