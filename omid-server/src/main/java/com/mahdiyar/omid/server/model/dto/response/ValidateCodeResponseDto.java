package com.mahdiyar.omid.server.model.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * Created by mahdiyar on 6/21/18.
 */
@Data
@NoArgsConstructor
public class ValidateCodeResponseDto {
    private String token;
    private Map<String, String> tags;

    public ValidateCodeResponseDto(String token, Map<String, String> tags) {
        this.token = token;
        this.tags = tags;
    }
}
