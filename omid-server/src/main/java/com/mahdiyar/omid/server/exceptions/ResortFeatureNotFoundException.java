package com.mahdiyar.omid.server.exceptions;

import com.mahdiyar.omid.server.model.annotations.HandledException;
import com.mahdiyar.omid.server.model.enums.ServiceExceptionEnumerator;

/**
 * Created by mahdiyar on 9/4/18.
 */
@HandledException(enumerator = ServiceExceptionEnumerator.ResortFeatureNotFound)
public class ResortFeatureNotFoundException extends ServiceException {
    public ResortFeatureNotFoundException() {
        super("Resort Feature Not Found");
    }
}
