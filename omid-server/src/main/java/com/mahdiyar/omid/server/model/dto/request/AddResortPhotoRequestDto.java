package com.mahdiyar.omid.server.model.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by mahdiyar on 8/16/18.
 */
@Data
@NoArgsConstructor
public class AddResortPhotoRequestDto {
    private List<String> photoMediaIds;
}
