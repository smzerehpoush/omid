package com.mahdiyar.omid.server.utils;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.Base64Utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Map;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by mahdiyar on 6/6/18.
 */
public class GeneralUtils {
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    private static final Pattern COUNTRY_PRECODE_PATTERN = Pattern.compile("\\+?(\\d{1,5})");

    public static String generateRandomDigits(int length) {
        SecureRandom random = new SecureRandom();
        int rNum = random.nextInt((int) Math.pow(10, length) - 1);
        return StringUtils.leftPad(String.valueOf(rNum), 4, '0');
    }

    public static boolean validateEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

    public static String getUrlEncodedParams(Map<String, ?> params) {
        StringJoiner joiner = new StringJoiner("&");
        try {
            for (Map.Entry<String, ?> entry : params.entrySet()) {
                String paramName = entry.getKey();
                String value = String.valueOf(entry.getValue());
                String valueUrlEncoded = URLEncoder.encode(value, "UTF-8");
                joiner.add(paramName + "=" + valueUrlEncoded);
            }
        } catch (UnsupportedEncodingException e) {
            return null;
        }
        return joiner.toString();
    }

    public static String convertPersianDigitsToStandard(String str) {
        return str
                .replace("\u06f1", "1")
                .replace("\u06f2", "2")
                .replace("\u06f3", "3")
                .replace("\u06f4", "4")
                .replace("\u06f5", "5")
                .replace("\u06f6", "6")
                .replace("\u06f7", "7")
                .replace("\u06f8", "8")
                .replace("\u06f9", "9")
                .replace("\u06f0", "0");
    }

    public static String getMd5HashBase64(String str) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(str.getBytes());
            byte[] md5Bytes = md.digest();
            return Base64Utils.encodeToString(md5Bytes);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getMd5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(input.getBytes());
            byte[] md5Bytes = md.digest();
            return new String(Hex.encodeHex(md5Bytes));

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean validatePreCode(String preCode) {
        assert preCode != null;
        Matcher matcher = COUNTRY_PRECODE_PATTERN.matcher(preCode);
        return matcher.matches();
    }

    public static String getStandardPreCode(String preCode, boolean checkValidity) {
        assert preCode != null;
        Matcher matcher = COUNTRY_PRECODE_PATTERN.matcher(preCode);
        if (checkValidity && !validatePreCode(preCode)) {
            return null;
        }
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }
}
