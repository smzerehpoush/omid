package com.mahdiyar.omid.server.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by mahdiyar on 11/23/18.
 */
@Table(name = "notification")
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NotificationEntity extends BaseEntity {
    @Column(name = "title")
    private String title;
    @Column(name = "sub_title")
    private String subTitle;
    @Column(name = "body", columnDefinition = "text not null")
    private String body;
    @ManyToOne
    @JoinColumn(name = "sender_user_id")

    private UserEntity senderUser;
    @ManyToOne
    @JoinColumn(name = "filter_country_id")

    private CountryEntity filterCountry;
}
