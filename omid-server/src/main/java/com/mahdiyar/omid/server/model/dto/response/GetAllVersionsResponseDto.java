package com.mahdiyar.omid.server.model.dto.response;

import com.mahdiyar.omid.server.model.dto.VersionDto;
import com.mahdiyar.omid.server.model.entity.VersionEntity;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mahdiyar on 11/7/18.
 */
@Data
public class GetAllVersionsResponseDto {
    private List<VersionDto> versions;

    public GetAllVersionsResponseDto(List<VersionEntity> versions) {
        this.versions = new ArrayList<>();
        for (VersionEntity versionEntity : versions) {
            this.versions.add(new VersionDto(versionEntity));
        }
    }
}
