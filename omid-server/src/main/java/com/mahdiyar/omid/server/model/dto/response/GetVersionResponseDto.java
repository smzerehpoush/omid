package com.mahdiyar.omid.server.model.dto.response;

import com.mahdiyar.omid.server.model.dto.VersionDto;
import com.mahdiyar.omid.server.model.entity.VersionEntity;
import lombok.Data;

/**
 * Created by mahdiyar on 11/7/18.
 */
@Data
public class GetVersionResponseDto extends VersionDto {
    public GetVersionResponseDto(VersionEntity versionEntity) {
        super(versionEntity);
    }
}
