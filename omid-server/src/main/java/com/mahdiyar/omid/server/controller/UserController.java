package com.mahdiyar.omid.server.controller;

import com.google.common.collect.ImmutableMap;
import com.mahdiyar.omid.server.exceptions.*;
import com.mahdiyar.omid.server.model.RequestContext;
import com.mahdiyar.omid.server.model.RestResponse;
import com.mahdiyar.omid.server.model.dto.UserDto;
import com.mahdiyar.omid.server.model.dto.request.*;
import com.mahdiyar.omid.server.model.dto.response.*;
import com.mahdiyar.omid.server.model.entity.ActivationCodeEntity;
import com.mahdiyar.omid.server.model.entity.SessionEntity;
import com.mahdiyar.omid.server.model.entity.UserEntity;
import com.mahdiyar.omid.server.model.enums.UserRole;
import com.mahdiyar.omid.server.security.AuthRequired;
import com.mahdiyar.omid.server.security.RestApiAuthenticationInterceptor;
import com.mahdiyar.omid.server.services.GoogleService;
import com.mahdiyar.omid.server.services.MessageService;
import com.mahdiyar.omid.server.services.NotificationService;
import com.mahdiyar.omid.server.services.UserService;
import com.mahdiyar.omid.server.utils.Pair;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * Created by mahdiyar on 6/6/18.
 */
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private RequestContext requestContext;
    @Autowired
    private MessageService messageService;
    @Autowired
    private GoogleService googleService;

    @ApiOperation(value = "Signup user by email and password or by phone number.", notes = "phone number mus be in format 9121111111")
    @PostMapping("/signup")
    public RestResponse<UserSignUpResponseDto> signup(@RequestBody UserSignUpRequestDto request) throws InvalidDataException, SendSmsException, InsufficientDataProvidedException, UsernameAlreadyTakenException, InvalidUsernameException, CountryNotFoundException, UserByMobileNoAlreadyExists, UserByEmailAlreadyExists {
        return request.isSignUpByEmailAndPassword() ?
                signUpByEmailAndPassword(request.getEmail(), request.getPassword(), request.getMobileNo(), request.getCountryId()) :
                signUpByMobileNumber(request.getMobileNo(), request.getCountryId());

    }

    @ApiOperation(value = "Signup user by username and password or by phone number.", notes = "phone number mus be in format 9121111111")
    @PostMapping("/signupByUsername")
    public RestResponse<UserSignUpResponseDto> signupByUsername(@RequestBody UserSignUpRequestDto request) throws InvalidDataException, SendSmsException, InsufficientDataProvidedException, UsernameAlreadyTakenException, InvalidUsernameException, CountryNotFoundException, UserByMobileNoAlreadyExists, UserByEmailAlreadyExists {
        return signUpByUsernameAndPassword(request.getUsername(), request.getPassword(), request.getMobileNo(), request.getCountryId());

    }

    private RestResponse<UserSignUpResponseDto> signUpByEmailAndPassword(String email, String password, String mobileNo, String countryId) throws InsufficientDataProvidedException, CountryNotFoundException, UserByMobileNoAlreadyExists, InvalidDataException, UsernameAlreadyTakenException, UserByEmailAlreadyExists {
        UserEntity userEntity = userService.signupByEmailAndPassword(email, password, mobileNo, countryId, UserRole.User);
        return RestResponse.ok(new UserSignUpResponseDto(userEntity, messageService));
    }

    private RestResponse<UserSignUpResponseDto> signUpByUsernameAndPassword(String username, String password, String mobileNo, String countryId) throws InsufficientDataProvidedException, CountryNotFoundException, UserByMobileNoAlreadyExists, InvalidDataException, UsernameAlreadyTakenException, UserByEmailAlreadyExists {
        UserEntity userEntity = userService.signupByUsernameAndPassword(username, password, mobileNo, countryId, UserRole.User);
        return RestResponse.ok(new UserSignUpResponseDto(userEntity, messageService));
    }

    private RestResponse<UserSignUpResponseDto> signUpByMobileNumber(String mobileNo, String countryId) throws InsufficientDataProvidedException, CountryNotFoundException, SendSmsException, InvalidUsernameException, InvalidDataException, UsernameAlreadyTakenException {
        ActivationCodeEntity activationCodeEntity = userService.signupByMobileNo(mobileNo, countryId, UserRole.User);
        return RestResponse.ok(new UserSignUpResponseDto(activationCodeEntity, messageService));
    }

    @ApiOperation("Validate the sent code by sms or email")
    @PostMapping("/validateCode")
    public RestResponse<ValidateCodeResponseDto> validateCode(@RequestBody ValidateCodeRequestDto request,
                                                              HttpServletResponse response) throws ConsumedActivationCodeException, ExpiredActivationCodeException, ActivationCodeMismatchException, UserNotFoundException, GenerateTokenException, NoActivationCodeGeneratedException, CountryNotFoundException, VersionNotFoundException, InsufficientDataProvidedException {
        Pair<SessionEntity, String> result = userService.validateUserByActivationCode(request.getMobileNo(),
                request.getCountryId(),
                request.getActivationCode(),
                request.getDeviceId(),
                request.getDeviceInfo(),
                request.getPlatform(),
                request.getBuildNo(),
                requestContext.getClientIp()
        );
        SessionEntity session = result.getLeft();
        String token = result.getRight();
        Cookie cookie = new Cookie(RestApiAuthenticationInterceptor.COOKIE_TOKEN, result.getRight());
        int age = session.getExpirationDate() != null ? (int) (session.getExpirationDate().getTime() - System.currentTimeMillis()) / 1000 : Integer.MAX_VALUE;
        cookie.setMaxAge(age);
        cookie.setPath("/");
        response.addCookie(cookie);
        Map<String, String> tags = ImmutableMap.of(NotificationService.TAG_KEY_MOBILE_NUMBER, String.valueOf(session.getUser().getMobileNo()),
                NotificationService.TAG_KEY_EMAIL, String.valueOf(session.getUser().getEmail()),
                NotificationService.TAG_KEY_COUNTRY_ID, String.valueOf(session.getUser().getCountry() != null ? session.getUser().getCountry().getId() : null));
        return RestResponse.ok(new ValidateCodeResponseDto(token, tags));
    }

    @ApiOperation("Edit the profile for user himself")
    @PostMapping("/profile")
    @AuthRequired
    public RestResponse<UpdateProfileResponseDto> updateProfile(@RequestBody UpdateProfileRequestDto request) throws MediaNotFoundException, InsufficientDataProvidedException, UsernameChangeNotAllowedException, InvalidUsernameException, UsernameAlreadyTakenException {
        UserEntity userEntity = userService.updateProfile(requestContext.getUser(), request.getUsername(), request.getFullName(), request.getProfilePictureMediaId(), request.getBio());
        return RestResponse.ok(new UpdateProfileResponseDto(userEntity, messageService));
    }

    @ApiOperation("Checks if username exists for being signed up")
    @PostMapping("/usernameExists")
    @AuthRequired
    public RestResponse<UsernameExistsResponseDto> usernameExists(@RequestBody UsernameExistsRequestDto request) throws InvalidUsernameException, UsernameAlreadyTakenException, InsufficientDataProvidedException, UserNotFoundException {
        return RestResponse.ok(userService.checkUsernameExistsForUser(requestContext.getUser(), request.getUsername()));
    }

    @ApiOperation("Login by username and password")
    @PostMapping("/login")
    public RestResponse<LoginResponseDto> login(@RequestBody LoginRequestDto request,
                                                HttpServletRequest httpServletRequest) throws UserNotFoundException, InsufficientDataProvidedException, WrongPasswordException, GenerateTokenException, UserNotValidatedException, VersionNotFoundException {
        String deviceInfo = httpServletRequest.getHeader("User-Agent");
        String deviceId = httpServletRequest.getRemoteAddr();
        String ip = httpServletRequest.getRemoteAddr();
        Pair<SessionEntity, String> response = userService.loginByUsernameAndPassword(request.getUsername(), request.getPassword(), deviceId, deviceInfo, ip, request.getPlatform(), request.getBuildNo());
        return RestResponse.ok(new LoginResponseDto(response.getLeft(), response.getRight(), messageService));
    }

    @ApiOperation(value = "Get users by user's country", notes = "Admin Auth Required")
    @GetMapping
    @AuthRequired(roles = UserRole.Admin)
    public RestResponse<GetAllUsersResponseDto> getAllUsers(@RequestParam(value = "countryId", required = false) String countryId) {
        List<UserEntity> users = userService.getUsersByCountryId(countryId);
        return RestResponse.ok(new GetAllUsersResponseDto(users, messageService));
    }

    @ApiOperation(value = "active/deactive user", notes = "Admin Auth Required")
    @PostMapping("{userId}/active")
    @AuthRequired(roles = UserRole.Admin)
    public RestResponse<UserDto> activateUser(@PathVariable("userId") String userId) throws UserNotFoundException {
        UserEntity userEntity = userService.activateUser(userId);
        return RestResponse.ok(new UpdateProfileResponseDto(userEntity, messageService));
    }

    @ApiOperation(value = "update user phone number", notes = "Admin Auth Required")
    @PostMapping("{phoneNumber}/phoneNumber")
    @AuthRequired(roles = UserRole.Admin)
    public RestResponse<UserDto> updateUserPhoneNumber(@PathVariable("phoneNumber") String phoneNumber, @RequestParam(value = "newPhoneNumber", required = true) String newPhoneNumber) throws UserNotFoundException, UserByMobileNoAlreadyExists {
        UserEntity userEntity = userService.updateUserPhoneNumber(phoneNumber, newPhoneNumber);
        return RestResponse.ok(new UpdateProfileResponseDto(userEntity, messageService));
    }

    @ApiOperation("google login")
    @PostMapping("/googleLogin")
    public RestResponse<GoogleResponseDto> googleLogin(@RequestBody GoogleRequestDto request, HttpServletResponse response) throws InvalidDataException, InsufficientDataProvidedException, GoogleAuthException, CountryNotFoundException {
        Pair<SessionEntity, String> result = googleService.googleSignupOrSignIn(request.getToken(), request.getCountryId(), request.getDeviceId(), request.getDeviceInfo(), request.getPlatform(), request.getBuildNo(), requestContext.getClientIp());

        SessionEntity session = result.getLeft();
        String apptoken = result.getRight();
        Cookie cookie = new Cookie(RestApiAuthenticationInterceptor.COOKIE_TOKEN, result.getRight());
        int age = session.getExpirationDate() != null ? (int) (session.getExpirationDate().getTime() - System.currentTimeMillis()) / 1000 : Integer.MAX_VALUE;
        cookie.setMaxAge(age);
        cookie.setPath("/");
        response.addCookie(cookie);
        Map<String, String> tags = ImmutableMap.of(NotificationService.TAG_KEY_MOBILE_NUMBER, String.valueOf(session.getUser().getMobileNo()),
                NotificationService.TAG_KEY_EMAIL, String.valueOf(session.getUser().getEmail()),
                NotificationService.TAG_KEY_COUNTRY_ID, String.valueOf(session.getUser().getCountry() != null ? session.getUser().getCountry().getId() : null));
        return RestResponse.ok(new GoogleResponseDto(apptoken, session.getUser().getUsername(), session.getUser().getFullName(), session.getUser().getId(), tags, session.getUser().getBio(), session.getUser().getVerified()));
    }

    @ApiOperation("return list of users with given search phrase - 15 items")
    @GetMapping("/username/{username}")
    @AuthRequired
    public RestResponse<SearchByUsernameResponseDto> searchByUsername(@PathVariable(value = "username") String username) throws CountryNotFoundException {
        return RestResponse.ok(userService.searchUsersByUsername(username, requestContext.getUser()));
    }

    @ApiOperation("add new currencyExchange to user")
    @PostMapping("/currency")
    public RestResponse<Void> addNewCurrency(@RequestBody AddNewCurrencyRequestDto request) throws CountryNotFoundException, InsufficientDataProvidedException, SessionNotFoundException {
        userService.addNewCurrency(request);
        return RestResponse.ok();
    }

    @ApiOperation("delete new currencyExchange to user")
    @PostMapping("/currencyd")
    public RestResponse<Void> deleteUserCurrency(@RequestBody RemoveCurrencyRequestDto request) throws CountryNotFoundException, InsufficientDataProvidedException, SessionNotFoundException {
        userService.deleteUserCurrency(request);
        return RestResponse.ok();
    }

    @ApiOperation("get list of user currency exchanges")
    @PutMapping("/currency")
    public RestResponse<List<UserCurrencyResponseDto>> getUserCurrencyExchanges(@RequestBody GetUserCurrencyExchangesRequestDto request) throws CountryNotFoundException, SessionNotFoundException {
        return RestResponse.ok(userService.findUserCurrencyExchanges(request.getToken()));
    }
}
