package com.mahdiyar.omid.server.controller;

import com.mahdiyar.omid.server.exceptions.CountryNotFoundException;
import com.mahdiyar.omid.server.exceptions.InsufficientDataProvidedException;
import com.mahdiyar.omid.server.model.RequestContext;
import com.mahdiyar.omid.server.model.RestResponse;
import com.mahdiyar.omid.server.model.dto.request.SendPushNotificationRequestDto;
import com.mahdiyar.omid.server.model.dto.response.GetNotificationsForAdminResponseDto;
import com.mahdiyar.omid.server.model.dto.response.GetNotificationsForUsersResponseDto;
import com.mahdiyar.omid.server.model.dto.response.SendNotificationResponseDto;
import com.mahdiyar.omid.server.model.entity.NotificationEntity;
import com.mahdiyar.omid.server.model.enums.UserRole;
import com.mahdiyar.omid.server.security.AuthRequired;
import com.mahdiyar.omid.server.services.NotificationService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

/**
 * Created by mahdiyar on 11/19/18.
 */
@RestController
@RequestMapping("/api/notification")
public class NotificationController {
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private RequestContext requestContext;


    @ApiOperation(value = "Get User's Notifications", notes = "User Auth Required, 'page' param starts with 0")
    @GetMapping
    @AuthRequired(roles = UserRole.User)
    public RestResponse<GetNotificationsForUsersResponseDto> getNotificationsForUsers(@RequestParam(value = "fromDate", required = false) Long fromDate,
                                                                                      @RequestParam(value = "toDate", required = false) Long toDate,
                                                                                      @RequestParam("page") int page,
                                                                                      @RequestParam("limit") int limit) {
        Page<NotificationEntity> notifications = notificationService.getUserNotifications(requestContext.getUser(), fromDate, toDate, page, limit);
        return RestResponse.ok(new GetNotificationsForUsersResponseDto(notifications));
    }

    @ApiOperation(value = "Send Push Notification", notes = "Admin Auth Required")
    @PostMapping
    @AuthRequired(roles = UserRole.Admin)
    public RestResponse<SendNotificationResponseDto> sendNotification(@RequestBody SendPushNotificationRequestDto request) throws CountryNotFoundException, InsufficientDataProvidedException {
        NotificationEntity notification = notificationService.sendNotification(
                request.getTitle(),
                request.getSubTitle(),
                request.getBody(),
                request.getCountryId(),
                requestContext.getUser());
        return RestResponse.ok(new SendNotificationResponseDto(notification));
    }

    @ApiOperation(value = "Get Notifications for Admin", notes = "Admin Auth Required")
    @GetMapping("/admin")
    @AuthRequired(roles = UserRole.Admin)
    public RestResponse<GetNotificationsForAdminResponseDto> getNotificationsForAdmin(@RequestParam(value = "fromDate", required = false) Long fromDate,
                                                                                      @RequestParam(value = "toDate", required = false) Long toDate,
                                                                                      @RequestParam(value = "countryId", required = false) String countryId,
                                                                                      @RequestParam("page") int page,
                                                                                      @RequestParam("limit") int limit) {
        Page<NotificationEntity> notifications = notificationService.searchAllNotifications(countryId, fromDate, toDate, page, limit);
        return RestResponse.ok(new GetNotificationsForAdminResponseDto(notifications));
    }

    @ApiOperation(value = "Delete notification", notes = "Admin Auth Required")
    @AuthRequired(roles = UserRole.Admin)
    @DeleteMapping("/{notificationId}")
    public RestResponse<Void> deleteNotification(@PathVariable("notificationId") String notificationId) {
        notificationService.deleteById(notificationId);
        return RestResponse.ok();
    }
}
