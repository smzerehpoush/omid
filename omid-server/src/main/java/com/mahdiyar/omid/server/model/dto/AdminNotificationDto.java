package com.mahdiyar.omid.server.model.dto;

import com.mahdiyar.omid.server.model.entity.NotificationEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Created by mahdiyar on 11/19/18.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class AdminNotificationDto extends NotificationDto {
    private BriefUserDto senderUser;
    private CountryDto filterCountry;

    public AdminNotificationDto(NotificationEntity notificationEntity) {
        super(notificationEntity);
        this.senderUser = notificationEntity.getSenderUser() != null ? new BriefUserDto(notificationEntity.getSenderUser()) : null;
        this.filterCountry = notificationEntity.getFilterCountry() != null ? new CountryDto(notificationEntity.getFilterCountry()) : null;
    }
}
