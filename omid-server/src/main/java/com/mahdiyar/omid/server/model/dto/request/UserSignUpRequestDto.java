package com.mahdiyar.omid.server.model.dto.request;

import lombok.Data;

/**
 * Created by mahdiyar on 6/21/18.
 */
@Data
public class UserSignUpRequestDto {
    private String mobileNo;
    private String username;
    private String countryId;
    private String email;
    private String password;
    private boolean signUpByEmailAndPassword;
}
