package com.mahdiyar.omid.server.dao;

import com.mahdiyar.omid.server.model.entity.SmsLogEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by mahdiyar on 6/6/18.
 */
@Repository
public interface SmsLogRepo extends JpaRepository<SmsLogEntity, String> {
}
