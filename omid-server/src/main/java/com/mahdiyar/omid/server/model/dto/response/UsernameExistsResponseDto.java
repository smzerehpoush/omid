package com.mahdiyar.omid.server.model.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by mahdiyar on 7/14/18.
 */
@Data
@NoArgsConstructor
public class UsernameExistsResponseDto {
    private boolean result;
    private String dsc;

    public UsernameExistsResponseDto(boolean result, String dsc) {
        this.result = result;
        this.dsc = dsc;
    }
}
